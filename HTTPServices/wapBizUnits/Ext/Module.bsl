﻿//*** Контрагенты ***//
function indexGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.Контрагенты.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
			continue;
		endif;
		
		if select.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо then
			xml.WriteStartElement("Company");
		elsif select.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо then
			xml.WriteStartElement("Individual");
		else
			return wap.errorResponse(422, "Неизвестный вид котрагента - " + String(select.ЮридическоеФизическоеЛицо));
    endif;

		//xml.WriteStartElement("BizUnit");
		xml.WriteAttribute("oid",  String(select.ref.UUID()));
    //xml.WriteAttribute("id",  select.Код);
		xml.WriteAttribute("inn",  select.ИНН);
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); 	
		
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function indexPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Index" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	resultXml = wap.newXmlResponse();

	while wap.iterNextXmlElement(iter) do
		createOrUpdateBizUnit(iter, resultXml);

	enddo;		
	
	return wap.doneResponse(resultXml); //"<OK/>");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createOrUpdateBizUnit(xmlIter, resultXml)
	iter = xmlIter;
	
  if iter.tag <> "Company" and iter.tag <> "Individual" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
  resultXml.WriteStartElement(iter.tag);
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;	
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	
try	
	oid = new UUID(iter.attributes.oid);
	ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
	isNew = wap.isEmptyRef(ref);
	if isNew then
		obj = Справочники.Контрагенты.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
	else
		obj = ref.GetObject();
	endif;	
		
	if iter.tag = "Company" then
		obj.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо;
	else
		obj.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
  endif;
		
	if iter.attributes.Property("id") = true then
		obj.Код = iter.attributes.id;
	endif;	
	if iter.attributes.Property("inn") = true then
		obj.ИНН = iter.attributes.inn;
	endif;	
	obj.Наименование = iter.text;

	obj.Write();
		
	wap.setExtraAttribute(ref, "ImportDate",  String(ТекущаяДата()));
	
		
	resultXml.WriteText(?(isNew, "создан", "обновлен"));
	resultXml.WriteEndElement(); // Stock
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Stock
endtry;
endprocedure


//*** Наши фирмы ***//
function ourfirmsGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.Организации.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
			continue;
		endif;
		
		if select.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо then
			xml.WriteStartElement("Company");
		elsif select.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо then
			xml.WriteStartElement("Individual");
		else
			return wap.errorResponse(422, "Неизвестный вид котрагента - " + String(select.ЮридическоеФизическоеЛицо));
    endif;

		//xml.WriteStartElement("BizUnit");
		xml.WriteAttribute("oid",  String(select.ref.UUID()));
    //xml.WriteAttribute("id",  select.Код);
		xml.WriteAttribute("inn",  select.ИНН);
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); 	
		
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function ourfirmsPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Index" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	
	resultXml = wap.newXmlResponse();

	while wap.iterNextXmlElement(iter) do
		if iter.tag <> "Company" and iter.tag <> "Individual" then
			return wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
		endif;
		if iter.attributes.Property("oid") = false then
			return wap.errorResponse(422, "Ошибка формата XML запроса: oid не указан!");
		endif;	
		
		oid = iter.attributes.oid; //new UUID(iter.attributes.oid);
		inn = iter.attributes.inn;
		
		resultXml.WriteStartElement("Org");
		resultXml.WriteAttribute("oid",  oid);
		
		orgRef = wap.getOrganization(oid, false);
		if orgRef <> undefined then
			if orgRef.ИНН = inn then
				resultXml.WriteText("по oid найден; инн совпадает");
			else
				resultXml.WriteText("по oid найден; ИНН НЕ СОВПАДАЕТ !!!");
			endif;
			resultXml.WriteEndElement(); // Org
			continue;
		endif;
		
		orgRef = Справочники.Организации.НайтиПоРеквизиту("ИНН", inn);
		if orgRef = Справочники.Организации.ПустаяСсылка() then
			resultXml.WriteText("ПО OID НЕ НАЙДЕН; НЕ НАЙДЕН по ИНН !!!");
		elsif wap.getOID(orgRef) = undefined then
			wap.setOID(orgRef, oid);
			resultXml.WriteText("ПО OID НЕ НАЙДЕН; сопоставлен по ИНН");
		else
			resultXml.WriteText("!!! НАЙДЕН по ИНН, НО С ДРУГИМ OID !!!");
		endif;
		resultXml.WriteEndElement(); // Org
	enddo;		
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction


//=== OLD ===

//*** groups - дерево групп контрагентов ***//
function groupsGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.Контрагенты.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = false then
			continue;
		endif;
		xml.WriteStartElement("BizUnitGroup");
		xml.WriteAttribute("oid",  String(select.ref.UUID()));
		//xml.WriteAttribute("code",  String(select.Код));
		xml.WriteAttribute("pid",  String(select.Parent.ref.UUID()));
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); 
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction
function groupsPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;

	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	
	codes = new Map();
	select = Справочники.Контрагенты.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = false then
			continue;
		endif;
		codes.Insert(select.ref.UUID(), select.ref);
	enddo; 
	
	while wap.iterNextXmlElement(iter) = true do
		if iter.tag <> "BizUnitGroup" then
			return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
		endif;
		
		oid = new UUID(iter.attributes.oid);
		ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			obj = Справочники.Контрагенты.СоздатьГруппу();
			obj.УстановитьСсылкуНового(ref); 
			obj.Наименование = iter.text;
			obj.Parent = Справочники.Контрагенты.ПолучитьСсылку(new UUID(iter.attributes.pid));
			obj.Write();
		elsif ref.Наименование <> iter.text or ref.Parent.ref.UUID() <> (new UUID(iter.attributes.pid)) then
			obj = ref.ПолучитьОбъект();
			obj.Наименование = iter.text;
			obj.Parent = Справочники.Контрагенты.ПолучитьСсылку(new UUID(iter.attributes.pid));
			obj.Write();
  	endif;
	
		codes.Delete(oid);
	enddo;
	
	for each doDel in codes do
		code = doDel.Value.ПолучитьОбъект();
		code.ПометкаУдаления = true;
		code.Write();
	enddo;
	
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

//*** Контрагенты ***//
function opIndexGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "list" then
		select = Справочники.Контрагенты.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true  or select.ЭтоГруппа = true then
				continue;
			endif;
			bizunitToXml(select, xml);
		enddo;                              
	else
		oid = new UUID(oid);
		ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		if ref.IsFolder = true then
			return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
		endif;
		bizunitToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function opIndexPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		obj = Справочники.Контрагенты.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
		obj.Write();
  endif;
	
	return opIndexPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function opIndexPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "BizUnit" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	obj = ref.GetObject();
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "Name" then
			obj.Наименование = iter.text;
		elsif iter.tag = "Kind" then
			obj.ЮридическоеФизическоеЛицо = wap.enumFromString(Перечисления.ЮридическоеФизическоеЛицо, iter.text);
		elsif iter.tag = "FullName" then
			obj.НаименованиеПолное = iter.text;
		elsif iter.tag = "Country" then
			cref = Справочники.СтраныМира.НайтиПоКоду(iter.text);
			if wap.isEmptyRef(cref) = false then
				obj.СтранаРегистрации = cref;
			endif;
		elsif iter.tag = "BizUnitGroup" then
			gref = Справочники.Контрагенты.ПолучитьСсылку(new UUID(iter.text));
			obj.Родитель = gref;
		elsif iter.tag = "INN" then
			obj.ИНН = iter.text;
		elsif iter.tag = "KPP" then
			obj.КПП = iter.text;
		elsif iter.tag = "Passport" then
			obj.ДокументУдостоверяющийЛичность = iter.text;
		elsif iter.tag = "OGRN" then
			obj.РегистрационныйНомер = iter.text;
		elsif iter.tag = "Comment" then
			obj.Комментарий = iter.text;
		elsif iter.tag = "BankAccount" then
			if StrLen(iter.attributes.pc) = 0 then
				if wap.isEmptyRef(obj.ОсновнойБанковскийСчет) = false then
					obj.ОсновнойБанковскийСчет = undefined;
				endif;
			else
				if wap.isEmptyRef(obj.ОсновнойБанковскийСчет) or 
					 obj.ОсновнойБанковскийСчет.НомерСчета <> iter.attributes.pc then
					bill = Справочники.БанковскиеСчета.СоздатьЭлемент();
					bill.НомерСчета = iter.attributes.pc;
					bill.Владелец = obj.ref;
					bill.Write();
					obj.ОсновнойБанковскийСчет = bill.ref;
				//else
				//	bill = obj.ОсновнойБанковскийСчет.ПолучитьОбъект();
				endif;
				
				if StrLen(iter.attributes.bik) > 0 then
					bankRef = Справочники.Банки.НайтиПоКоду(iter.attributes.bik);
					if wap.isEmptyRef(bankRef) then
						bank = Справочники.Банки.СоздатьЭлемент();
						bank.Код = iter.attributes.bik;
						bank.Write();
						bankRef = bank.ref;
					endif;
					if bankRef.Наименование <> ?(iter.text = undefined, "", iter.text) or
						 bankRef.КоррСчет <> iter.attributes.kc then
						bank = bankRef.ПолучитьОбъект();
						bank.Наименование = iter.text;
						bank.КоррСчет = iter.attributes.kc;
						bank.Write();
					endif;
					if wap.isEmptyRef(obj.ОсновнойБанковскийСчет.Банк) or obj.ОсновнойБанковскийСчет.Банк <> bankRef then
						bill = obj.ОсновнойБанковскийСчет.ПолучитьОбъект();
						bill.Банк = bankRef;
						bill.Write();
					endif;
				endif;
			endif;
		elsif iter.tag = "Comment" then
			obj.Комментарий = iter.text;

		endif;
	enddo;
	obj.Write();
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function opIndexDELETE(Request)
	return wap.doDeleteByXml(request, Справочники.Контрагенты, "BizUnit");
endFunction

//--- Tools ---
procedure bizunitToXml(bizunit, xml)
	xml.WriteStartElement("BizUnit");
	xml.WriteAttribute("oid",  String(bizunit.ref.UUID()));
	
	//wap.writeTag(xml, "ID",  String(bizunit.Код));
	wap.writeTag(xml, "Kind", String(bizunit.ЮридическоеФизическоеЛицо));
	wap.writeTag(xml, "Name", bizunit.Наименование);
	wap.writeTag(xml, "FullName", bizunit.НаименованиеПолное);
	wap.writeTag(xml, "BizUnitGroup", String(bizunit.Родитель.ref.UUID()));
	wap.writeTag(xml, "Country", bizunit.СтранаРегистрации.Код);
	wap.writeTag(xml, "INN", bizunit.ИНН);
	if bizunit.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо then
		wap.writeTag(xml, "KPP", bizunit.КПП);
	else
		wap.writeTag(xml, "Passport", bizunit.ДокументУдостоверяющийЛичность);
	endif;
	wap.writeTag(xml, "OGRN", bizunit.РегистрационныйНомер);
	if StrLen(bizunit.Комментарий) > 0 then
		wap.writeTag(xml, "Comment", bizunit.Комментарий);
	endif;
	
	if wap.isEmptyRef(bizunit.ОсновнойБанковскийСчет) = false then
		xml.WriteStartElement("BankAccount");
		xml.WriteAttribute("pc", bizunit.ОсновнойБанковскийСчет.НомерСчета);
		xml.WriteAttribute("bik", bizunit.ОсновнойБанковскийСчет.Банк.Код);
		xml.WriteAttribute("kc", bizunit.ОсновнойБанковскийСчет.Банк.КоррСчет);
		xml.WriteText(bizunit.ОсновнойБанковскийСчет.Банк.Наименование);
		xml.WriteEndElement(); //"BankAccount");
	endif;
	
	for each contact in bizunit.КонтактнаяИнформация do
		if contact.Тип = Перечисления.ТипыКонтактнойИнформации.Адрес then
			if String(contact.Вид) = "Юридический адрес" then
				wap.writeTag(xml,"RegAddress", contact.Представление);
			elsif String(contact.Вид) = "Фактический адрес" then
				wap.writeTag(xml,"FactAddress", contact.Представление);
			elsif String(contact.Вид) = "Почтовый адрес" then
				wap.writeTag(xml,"MailAddress", contact.Представление);
			endif;
		elsif contact.Тип = Перечисления.ТипыКонтактнойИнформации.Телефон then
			wap.writeTag(xml,"Phone", contact.Представление); //.НомерТелефона);
		elsif contact.Тип = Перечисления.ТипыКонтактнойИнформации.АдресЭлектроннойПочты then
			wap.writeTag(xml,"Email", contact.Представление);
		endif;
	enddo; 
	
	xml.WriteEndElement(); 	
endprocedure

