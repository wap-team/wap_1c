﻿function moneyTransferGET(Request)
try
	xml = wap.newXmlResponse();          
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.ОперацияБух.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			if wap.getProcType(select.ref) <> "ProcessMoneyTransfer" then
				continue;
			endif;
			
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.ОперацияБух.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			if wap.getProcType(select.ref) <> "ProcessMoneyTransfer" then
				continue;
			endif;
			moneyTransferToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.ОперацияБух.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		moneyTransferToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function moneyTransferPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	moneyTransferCreate(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function moneyTransferPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	moneyTransferCreate(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
procedure moneyTransferCreate(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	if iter.attributes.Property("type") = false then
		raise("Ошибка формата XML запроса: атрибут type не указан!");
	endif;
	procType = iter.attributes.type;
	resultXml.WriteAttribute("proc_type",  procType);
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.ОперацияБух.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.ОперацияБух.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	
	
	//doc.Организация = wap.getCurrentUserOrganization();
	//doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Содержание = "Начисление зарплаты";
	doc.Комментарий = "из WAP";
	
  Движения = doc.Движения.Хозрасчетный;
  Движения.Записывать = Истина;
	item = Движения.Добавить();
	item.Активность = Истина;
	
	coven = undefined;
	isOurFirmPayment  = undefined;
	writeMode = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "Reason" then
      doc.Содержание = iter.text;
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "isOurFirmPayment" then
			isOurFirmPayment = Boolean(iter.text);		
	  elsif iter.tag = "Note" then
      doc.Комментарий = doc.Комментарий + " - " + iter.text;
		elsif iter.tag = "Covenantor" then	
			coven = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			//doc.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПоставщиком);
			//item.СубконтоКт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.РаботникиОрганизаций] = empl;
		elsif iter.tag = "DocTotal" then
			item.Сумма = Number(iter.text);
			doc.СуммаОперации = Number(iter.text);
		elsif iter.tag = "Debit" then	
			item.СчетДт = wap.accountFromString(iter.text); 
		elsif iter.tag = "Credit" then	
			item.СчетКт  =  wap.accountFromString(iter.text); 
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
		endif;
	enddo;
	
	if isOurFirmPayment = undefined then
		raise("Ошибка формата XML запроса: isOurFirmPayment не указан!");
	elsif isOurFirmPayment = true then
		doc.Содержание =  "Зачёт c " + coven.Наименование + " - " + doc.Содержание;
		item.СубконтоДт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = doc.Организация;
		item.СубконтоКт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = coven;
	else
		doc.Содержание =  "Зачёт от " + coven.Наименование + " - " + doc.Содержание;
		item.СубконтоДт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = coven;
		item.СубконтоКт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = doc.Организация;
	endif;
		
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	check = doc.ПроверитьЗаполнение();
	doc.Проведен = Истина;
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then
		doc.Write(РежимЗаписиДокумента.Запись); // РежимЗаписиДокумента.Проведение); - Особенность документа
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure moneyTransferToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
	
	doc = obj.ПолучитьОбъект();
	
	procOID = wap.getProcOID(doc.ref);
	if procOID <> undefined then
		wap.writeTag(xml, "ProcOID",  String(procOID));
	endif;
	procType = wap.getProcType(doc.ref);
	if procOID <> undefined then
		wap.writeTag(xml, "procType",  procType);
	endif;
	wap.writeTag(xml, "Number",  doc.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(doc.Дата));
  wap.writeTag(xml, "DocTotal", doc.СуммаОперации);
	wap.writeTag(xml, "Reason", doc.Содержание);
	wap.writeTag(xml, "Note", doc.Комментарий);
	
	xml.WriteStartElement("Items");
	Проводки =  doc.Движения.Хозрасчетный;
  Проводки.Прочитать();
  list = Проводки.Выгрузить();
	
	for each item in list do
		xml.WriteStartElement("Item");
		coven = item.СубконтоКт2;
		if wap.isEmptyRef(coven) = false then
			xml.WriteStartElement("Covenantor");
			xml.WriteAttribute("oid",  String(coven.UUID()));
			xml.WriteText(coven.Наименование);
			xml.WriteEndElement(); // Covenantor
	 	endif; 
		wap.writeTag(xml,"Debit", item.СчетДт);
		wap.writeTag(xml,"Credit", item.СчетКт);
		wap.writeTag(xml,"Summa", String(item.Сумма));
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure

function moneyNettingGET(Request)
try
	xml = wap.newXmlResponse();          
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.ОперацияБух.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			if wap.getProcType(select.ref) <> "ProcessMoneyNetting" then
				continue;
			endif;
			
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.ОперацияБух.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			if wap.getProcType(select.ref) <> "ProcessMoneyNetting" then
				continue;
			endif;
			moneyNettingToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.ОперацияБух.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		moneyNettingToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function moneyNettingPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	moneyNettingCreate(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function moneyNettingPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	moneyNettingCreate(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
procedure moneyNettingCreate(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	if iter.attributes.Property("type") = false then
		raise("Ошибка формата XML запроса: атрибут type не указан!");
	endif;
	procType = iter.attributes.type;
	resultXml.WriteAttribute("proc_type",  procType);
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.ОперацияБух.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.ОперацияБух.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	
	
	//doc.Организация = wap.getCurrentUserOrganization();
	//doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Содержание = "Начисление зарплаты";
	doc.Комментарий = "из WAP";
	doc.Содержание = "Взаимозачёт: ";
	
  Движения = doc.Движения.Хозрасчетный;
  Движения.Записывать = Истина;
	item = Движения.Добавить();
	item.Активность = Истина;
	
	payer = undefined;
	payee = undefined;
	writeMode = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "Reason" then
      doc.Содержание = doc.Содержание + iter.text;
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "Note" then
      doc.Комментарий = doc.Комментарий + " - " + iter.text;
		elsif iter.tag = "Payer" then	
			payer = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			item.СубконтоДт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = payer;
		elsif iter.tag = "Payee" then	
			payee = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			item.СубконтоКт[ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты] = payee;
		elsif iter.tag = "DocTotal" then
			item.Сумма = Number(iter.text);
			doc.СуммаОперации = Number(iter.text);
		elsif iter.tag = "Debit" then	
			item.СчетДт = wap.accountFromString(iter.text); 
		elsif iter.tag = "Credit" then	
			item.СчетКт  =  wap.accountFromString(iter.text); 
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
		endif;
	enddo;
	
	if doc.Содержание = "Взаимозачёт: " then
		doc.Содержание =  doc.Содержание + payer.Наименование + " -> " + payee.Наименование;
	endif;
		
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	check = doc.ПроверитьЗаполнение();
	doc.Проведен = Истина;
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then
		doc.Write(РежимЗаписиДокумента.Запись); // РежимЗаписиДокумента.Проведение); - Особенность документа
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure moneyNettingToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
	
	doc = obj.ПолучитьОбъект();
	
	procOID = wap.getProcOID(doc.ref);
	if procOID <> undefined then
		wap.writeTag(xml, "ProcOID",  String(procOID));
	endif;
	procType = wap.getProcType(doc.ref);
	if procOID <> undefined then
		wap.writeTag(xml, "procType",  procType);
	endif;
	wap.writeTag(xml, "Number",  doc.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(doc.Дата));
  wap.writeTag(xml, "DocTotal", doc.СуммаОперации);
	wap.writeTag(xml, "Reason", doc.Содержание);
	wap.writeTag(xml, "Note", doc.Комментарий);
	
	xml.WriteStartElement("Items");
	Проводки =  doc.Движения.Хозрасчетный;
  Проводки.Прочитать();
  list = Проводки.Выгрузить();
	
	for each item in list do
		xml.WriteStartElement("Item");
		coven = item.СубконтоКт2;
		if wap.isEmptyRef(coven) = false then
			xml.WriteStartElement("Covenantor");
			xml.WriteAttribute("oid",  String(coven.UUID()));
			xml.WriteText(coven.Наименование);
			xml.WriteEndElement(); // Covenantor
	 	endif; 
		wap.writeTag(xml,"Debit", item.СчетДт);
		wap.writeTag(xml,"Credit", item.СчетКт);
		wap.writeTag(xml,"Summa", String(item.Сумма));
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure

