﻿//*** Поступления на расчетный счет ***//
function receiptsGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.ПоступлениеНаРасчетныйСчет.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			docRef = select.ref;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(docRef.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  docRef.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(docRef.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.ПоступлениеНаРасчетныйСчет.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			receiptToXml(select.ref, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.ПоступлениеНаРасчетныйСчет.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		receiptToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function receiptsPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createReceipt(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function receiptsPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	                                 
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createReceipt(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function receiptsDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.ПоступлениеНаРасчетныйСчет.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createReceipt(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.ПоступлениеНаРасчетныйСчет.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.ПоступлениеНаРасчетныйСчет.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	
	
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	
	item = undefined;
	writeMode = undefined;
	procOID = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийПоступлениеДенежныхСредств, iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "FirmAccount" then
			acc = Справочники.БанковскиеСчета.НайтиПоРеквизиту("НомерСчета",iter.text); 
			if acc = undefined or wap.isEmptyRef(acc) then
				raise("Счет организации " + iter.text + " не найден!");
			endif;
			doc.СчетОрганизации = acc;
		elsif iter.tag = "BankDate" then
			doc.ДатаВходящегоДокумента = wap.dateFromISOString(iter.text);
		elsif iter.tag = "BankNumber" then
			doc.НомерВходящегоДокумента = iter.text;
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
		elsif iter.tag = "Reason" then
			doc.НазначениеПлатежа = iter.text;
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
			if doc.Контрагент = undefined then
				raise "Контрагент не определен к моменту определения договора!";
			endif;
			item = doc.РасшифровкаПлатежа.Добавить();
			item.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
			item.СуммаПлатежа = Number(iter.text);
			item.СуммаВзаиморасчетов = Number(iter.text);
			item.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			item.СпособПогашенияЗадолженности = Перечисления.СпособыПогашенияЗадолженности.НеПогашать;
			item.СчетУчетаРасчетовПоАвансам = wap.accountFromString("62.02");
		elsif iter.tag = "NdsRate" then
			item.СтавкаНДС = wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "AcquiringCommission" then
			doc.СуммаУслуг = Number(iter.text);
			doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString("91.02");  // 91.02 - коммисия банка
			if item <> undefined then
				item.СуммаУслуг = Number(iter.text);
			endif;
		elsif iter.tag = "Debit" then	
			doc.СчетБанк = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный;
		elsif iter.tag = "Credit" then	
			if doc.СуммаУслуг = 0 then
				doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); 
			endif;
			if item <> undefined then
				item.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text);
			endif;
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
		endif;
	enddo;
	
	if procOID = undefined then
		raise("Не указан ProcOID!");
	endif;
	if ЗначениеЗаполнено(doc.ВидОперации) = false then
		raise("Не указан вид операции!");
	endif;
	
	//doc.УстановитьНовыйНомер();
	if wap.isEmptyDate(doc.Дата) then
		doc.УстановитьВремя();
	endif;	
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "ProcOID",  String(wap.getProcOID(docRef)));
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure receiptToXml(obj, xml)
	docRef = obj;
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
  
	wap.writeTag(xml, "Number",  docRef.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	wap.writeTag(xml, "BankDate", wap.dateToISOString(docRef.ДатаВходящегоДокумента));
  wap.writeTag(xml, "BankNumber", docRef.НомерВходящегоДокумента);
	if wap.isEmptyRef(obj.СчетОрганизации) = false then
		 wap.writeTag(xml, "FirmAccount", obj.СчетОрганизации.НомерСчета);
	endif; 
	
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	endif; 

	wap.writeTag(xml, "NoReasonte", obj.НазначениеПлатежа);
	wap.writeTag(xml, "Note", obj.Комментарий);
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	
	xml.WriteStartElement("Items");
	for each item in obj.РасшифровкаПлатежа do
		xml.WriteStartElement("Item");
		wap.writeTag(xml,"Summa", item.СуммаПлатежа);
		wap.writeTag(xml,"NdsRate", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		if item.СуммаУслуг > 0 then
			wap.writeTag(xml,"AcquiringCommission", item.СуммаУслуг);
		endif;
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc
endprocedure

//*** Списания (оплаты) с расчетного счета ***//
function paymentsGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.СписаниеСРасчетногоСчета.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			docRef = select.ref;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(docRef.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  docRef.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(docRef.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.СписаниеСРасчетногоСчета.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			paymentToXml(select.ref, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.СписаниеСРасчетногоСчета.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		paymentToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function paymentsPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createPayment(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function paymentsPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	                                 
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createPayment(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function paymentsDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.СписаниеСРасчетногоСчета.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createPayment(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.СписаниеСРасчетногоСчета.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.СписаниеСРасчетногоСчета.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "ProcOID",  String(wap.getProcOID(docRef)));
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	
	
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	
	item = undefined;
	writeMode = undefined;
	procOID = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийСписаниеДенежныхСредств, iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);
		elsif iter.tag = "FirmAccount" then
			acc = Справочники.БанковскиеСчета.НайтиПоРеквизиту("НомерСчета",iter.text); 
			if acc = undefined or wap.isEmptyRef(acc) then
				raise("Счет организации " + iter.text + " не найден!");
			endif;
			doc.СчетОрганизации = acc;
		elsif iter.tag = "BankDate" then
			doc.ДатаВходящегоДокумента = wap.dateFromISOString(iter.text);
		elsif iter.tag = "BankNumber" then
			doc.НомерВходящегоДокумента = iter.text;
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
		elsif iter.tag = "Reason" then
			doc.НазначениеПлатежа = iter.text;
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
			if doc.Контрагент = undefined then
				raise "Контрагент не определен к моменту определения договора!";
			endif;
			item = doc.РасшифровкаПлатежа.Добавить();
			item.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
			item.СуммаПлатежа = Number(iter.text);
			item.СуммаВзаиморасчетов = Number(iter.text);
			item.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			item.СпособПогашенияЗадолженности = Перечисления.СпособыПогашенияЗадолженности.НеПогашать;
			item.СчетУчетаРасчетовПоАвансам = wap.accountFromString("60.02");
		elsif iter.tag = "NdsRate" then
			item.СтавкаНДС = wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "Debit" then
			doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); 
			if item <> undefined then
				item.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text);
			endif;
		elsif iter.tag = "Credit" then
			doc.СчетБанк = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный;
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
		endif;
	enddo;
	
	if procOID = undefined then
		raise("Не указан ProcOID!");
	endif;
	if ЗначениеЗаполнено(doc.ВидОперации) = false then
		raise("Не указан вид операции!");
	endif;
	
	//doc.УстановитьНовыйНомер();
	if wap.isEmptyDate(doc.Дата) then
		doc.УстановитьВремя();
	endif;	
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));

	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure paymentToXml(obj, xml)
	docRef = obj;
	
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
  procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;

	wap.writeTag(xml, "Number",  docRef.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	wap.writeTag(xml, "BankDate", wap.dateToISOString(docRef.ДатаВходящегоДокумента));
	wap.writeTag(xml, "BankNumber", docRef.НомерВходящегоДокумента);
	if wap.isEmptyRef(obj.СчетОрганизации) = false then
		 wap.writeTag(xml, "FirmAccount", obj.СчетОрганизации.НомерСчета);
	endif; 

	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	endif; 
	
	wap.writeTag(xml, "NoReasonte", obj.НазначениеПлатежа);
	wap.writeTag(xml, "Note", obj.Комментарий);
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	
	xml.WriteStartElement("Items");
	for each item in obj.РасшифровкаПлатежа do
		xml.WriteStartElement("Item");
		wap.writeTag(xml,"Summa", item.СуммаПлатежа);
		wap.writeTag(xml,"NdsRate", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc
endprocedure
