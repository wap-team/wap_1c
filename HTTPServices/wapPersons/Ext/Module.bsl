﻿
function listGET(Request)
try
	args = Request.QueryOptions; // wap.requestArgs(Request); 
	data = new Map();
	if args.get("data") <> undefined then
		//args["data"] = ?(IsBlankString(args["data"]), "person", args["data"]);
		for each x in StrSplit(args["data"], ",", false) do
			data[x] = true;
		enddo;
	endif;
		
	xml = wap.newXmlResponse();
	
	pers = Справочники.ФизическиеЛица.Выбрать();
	while pers.next()
	do
		if pers.IsFolder = true or pers.ПометкаУдаления = true then
			continue;
		endif;
		personToXml(pers, xml, data);
	enddo;                              

	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction

function opIndexGET(Request) // wap.requestArgs(Request);
try
	args = Request.QueryOptions;
	data = new Map();
	if args.get("data") <> undefined then
		//args["data"] = ?(IsBlankString(args["data"]), "person", args["data"]);
		for each x in StrSplit(args["data"], ",", false) do
			data[x] = true;
		enddo;
	endif;
		
	xml = wap.newXmlResponse(); 
	
	oid = new UUID(Request.URLParameters["oid"]);
	pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if wap.isEmptyRef(pers) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	if pers.IsFolder = true then
		return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
	endif;
	personToXml(pers, xml, data);

	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;

endFunction

function opIndexPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	persRef = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if persRef.IsFolder = true then
		return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
	endif;
	if wap.isEmptyRef(persRef) then
		pers = Справочники.ФизическиеЛица.СоздатьЭлемент();
		pers.УстановитьСсылкуНового(persRef); 
		pers.Write();
  endif;
	
	return opIndexPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

function opIndexPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if wap.isEmptyRef(pers) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	if pers.IsFolder = true then
		return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
	endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Person"  or iter.hasChilds = false then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return  wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	pers = pers.GetObject();
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "FullName" then
			pers.ФИО = iter.text;
			pers.Наименование = iter.text;
		elsif iter.tag = "Sex" then
			if iter.text = "Женский" then
				pers.Пол = Перечисления.ПолФизическогоЛица.Женский;
			elsif iter.text = "Мужской" then
				pers.Пол = Перечисления.ПолФизическогоЛица.Мужской;
			else 
				pers.Пол = undefined;
			endif;
		elsif iter.tag = "Birthday" then
	    pers.ДатаРождения = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Email" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.АдресЭлектроннойПочты);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.EMailФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
			c.ДоменноеИмяСервера = Сред(iter.text, StrFind(iter.text,"@")+1);
		elsif iter.tag = "PrivatePhone" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.Телефон);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.ТелефонДомашнийФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
			s = StrReplace(iter.text,"-","");
			s = StrReplace(s," ","");
			c.НомерТелефонаБезКодов = Сред(s,StrFind(s, ")")+1);
			s = StrReplace(s,"(","");
			s = StrReplace(s,")","");
			c.НомерТелефона = Сред(s,2);
		elsif iter.tag = "WorkPhone" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.Телефон);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.ТелефонРабочийФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
			s = StrReplace(iter.text,"-","");
			s = StrReplace(s," ","");
			int = StrFind(s, "вн.");
			s = ?(int > 0, Сред(s,1,int-1) , s);
			c.НомерТелефонаБезКодов = Сред(s,StrFind(s, ")")+1);
			s = StrReplace(s,"(","");
			s = StrReplace(s,")","");
			c.НомерТелефона = Сред(s,2);
		elsif iter.tag = "RegAddress" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.Адрес);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.АдресПоПропискеФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
		elsif iter.tag = "HomeAddress" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.Адрес);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.АдресМестаПроживанияФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
		elsif iter.tag = "InfoAddress" then
			select = new Structure();
			select.Insert("Тип", Перечисления.ТипыКонтактнойИнформации.Адрес);
			select.insert("Вид", Справочники.ВидыКонтактнойИнформации.АдресДляИнформированияФизическиеЛица);
			c = getPersonContactRow(pers,iter,select);
			if c = undefined then
				continue;
			endif;
			c.Представление = iter.text;
		endif;
	enddo;
	pers.Write();
	
	wap.setExtraAttribute(pers.ref, "ImportDate",  String(ТекущаяДата()));

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction



function setOIDGET(Request)
try
	args = wap.requestArgs(Request);
	code = args["id"];
	oidValue = args["oid"];
	if code = undefined OR IsBlankString(code) then
		raise "Не указан аргумент id!";
	elsif oidValue = undefined OR IsBlankString(oidValue) then
		raise "Не указан аргумент oid!";
	endif;
	
	pers = Справочники.ФизическиеЛица.НайтиПоКоду(code);
	if pers = undefined OR pers = Справочники.ФизическиеЛица.EmptyRef() then
		raise "Физическое лицо с кодом " + code + " не найдено!"; 
	endif;
		
	wap.setOID(pers,oidValue);
	
	xml = wap.newXmlResponse();
	xml.WriteText("OK");
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction

//--- Tools ---
procedure personToXml(person, xml, dataMap)
	xml.WriteStartElement("Person");
	xml.WriteAttribute("oid",  String(person.ref.UUID()));
		
		if dataMap["person"] = true then
			//wap.writeTag(xml, "OID", wap.getOID(person));
			wap.writeTag(xml, "FullName", person.ФИО);
		endif;
		
		if dataMap["1c"] = true then
			wap.writeTag(xml, "ID",  String(person.Код));
			//wap.writeTag(xml, "TimeStamp",  String(person.ВерсияДанных));
		endif;
		
		if dataMap["contact"] = true then
			if person.Пол.IsEmpty() = false then
				wap.writeTag(xml, "Sex", String(person.Пол));
			endif;
			if person.ДатаРождения <> '00010101' then
				wap.writeTag(xml, "Birthday", wap.dateToISOString(person.ДатаРождения));
			endif;
		endif;	
		
		if dataMap["contact"] = true or dataMap["1c"] = true then
			for each contact in person.КонтактнаяИнформация do
				if contact.Тип = Перечисления.ТипыКонтактнойИнформации.Телефон then
					if String(contact.Вид) = "Телефон домашний" then
						if dataMap["contact"] = true then
							wap.writeTag(xml,"PrivatePhone", contact.Представление); //.НомерТелефона);
						endif;	
						continue;
					elsif String(contact.Вид) = "Телефон рабочий" then
						if dataMap["contact"] = true then
							wap.writeTag(xml,"WorkPhone", contact.Представление); //НомерТелефона);
						endif;
					  continue;
					endif;
				elsif contact.Тип = Перечисления.ТипыКонтактнойИнформации.АдресЭлектроннойПочты then
					if dataMap["contact"] = true then
						wap.writeTag(xml,"Email", contact.Представление);
					endif;
				  continue;
				elsif contact.Тип = Перечисления.ТипыКонтактнойИнформации.Skype then
					if dataMap["contact"] = true then
						wap.writeTag(xml,"Skype", contact.Представление);
					endif;
				  continue;
				endif;
				
				if dataMap["1c"] = true then
					xml.WriteStartElement("IcInfo");
		 			xml.WriteAttribute("type", String(contact.Тип));
					xml.WriteAttribute("kind", String(contact.Вид));
					xml.WriteText(contact.Представление);
					xml.WriteEndElement();
				endif;
			enddo; 
		endif;
	xml.WriteEndElement(); 	
endprocedure
function getPersonContactRow(pers, iter, selectStruct)
	c = pers.КонтактнаяИнформация.FindRows(selectStruct);
	if c.Count() > 0 then
		if IsBlankString(iter.text) then
			pers.КонтактнаяИнформация.Delete(c[0]);
			return undefined;
		else
			c = c[0];
		endif;
	elsif IsBlankString(iter.text) = false then
		c = pers.КонтактнаяИнформация.Add();
		c.Тип = selectStruct.Тип;
		c.Вид = selectStruct.Вид;
	else
		return undefined;
	endif;
	return c;
endfunction




