﻿/// Возвращает информацию о сервере и 1С
function infoGET(Request)
try	
	xml = wap.newXmlResponse();
	
	xml.WriteStartElement("Server");
		sysInfo = new SystemInfo;
		wap.writeTag(xml, "PlatformVersion", sysInfo.AppVersion);
		wap.writeTag(xml, "PlatformType", String(sysInfo.PlatformType));
		wap.writeTag(xml, "ServerName", ComputerName());        
		wap.writeTag(xml, "OS", sysInfo.OSVersion);
		wap.writeTag(xml, "Processor", sysInfo.Processor);
		wap.writeTag(xml, "RAM", Format(sysInfo.RAM, "NG=9" ));
	xml.WriteEndElement(); // "Server" 
		
	xml.WriteStartElement("InfoBase");
		wap.writeTag(xml, "Name", Metadata.Name); 
		wap.writeTag(xml, "Synonym", Metadata.Synonym);
		wap.writeTag(xml, "Version", Metadata.Version);
		wap.writeTag(xml, "ConnectionString", InfoBaseConnectionString());
		wap.writeTag(xml, "BinDir", BinDir());
		wap.writeTag(xml, "TempFilesDir", TempFilesDir());
		wap.writeTag(xml, "UserName", UserName());
		wap.writeTag(xml, "ClientID", String(sysInfo.ClientID));
		wap.writeTag(xml, "UserAgentInformation", sysInfo.UserAgentInformation);
	xml.WriteEndElement(); // "Base"
	
	xml.WriteStartElement("WAP1C");
	  exts = РасширенияКонфигурации.Получить(Новый Структура("Имя", "WAP_1C"));
		if exts <> undefined and exts.Count() > 0 then
			ext = exts[0];
			if ext <> undefined then
				wap.writeTag(xml, "Name", ext.Name);
				wap.writeTag(xml, "Active", ext.Active);
				wap.writeTag(xml, "Version", ext.Version); 
				wap.writeTag(xml, "UUID", ext.UUID);
			endif;
		endif;
	xml.WriteEndElement(); // "WAP1C"
		
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
/// Возвращает список значений(строк) справочников
function indexRefGET(Request)
try
	xml = wap.newXmlResponse();
	
  ref = Справочники[Request.URLParameters["name"]];

	select = ref.Выбрать();
	while select.next()
	do
		xml.WriteStartElement("Item");
		xml.WriteAttribute("oid",  String(select.ref.UUID()));
		if select.IsFolder = true then
			xml.WriteAttribute("isFolder",  "");
		endif;
		if select.ПометкаУдаления = true then
			xml.WriteAttribute("deleted",  "");
		endif;
		xml.WriteAttribute("id", select.Код);
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); // Item
	enddo;                              
		
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
/// Возвращает список значений перечисления
function indexEnumGET(Request)
try
	xml = wap.newXmlResponse();
	
	enumName = Request.URLParameters["name"];
	for each kind in Метаданные.Перечисления[enumName].ЗначенияПеречисления
		do                      			
			xml.WriteStartElement("Kind");
			xml.WriteAttribute("name",  kind.Имя);
     	xml.WriteText(kind.Синоним);
			xml.WriteEndElement(); 
		enddo;   
	                      
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction

/// Конфигурирует сервер или возвращает 
function config_GET_PATCH(Request)
try
	setup = Request.HTTPМетод = "PATCH";
	
	xml = wap.newXmlResponse();

	// Включаем Дополнительные Реквизиты И Сведения
	ini = Константы.ИспользоватьДополнительныеРеквизитыИСведения.Получить();
	if ini = false AND setup = true then
		Константы.ИспользоватьДополнительныеРеквизитыИСведения.Установить(true);
		ini = Константы.ИспользоватьДополнительныеРеквизитыИСведения.Получить();
	endif;	
	wap.writeTag(xml, "ИспользоватьДополнительныеРеквизитыИСведения", String(ini));
	if ini = false then
	 return wap.doneResponse(xml);
	endif;	
	
	//// Включаем Общие Дополнительные Реквизиты И Сведения
	//ini = Константы.ИспользоватьОбщиеДополнительныеРеквизитыИСведения.Получить();
	//if ini = false AND setup = true then
	//	Константы.ИспользоватьОбщиеДополнительныеРеквизитыИСведения.Установить(true);
	//	ini = Константы.ИспользоватьОбщиеДополнительныеРеквизитыИСведения.Получить();
	//endif;	
	//wap.writeTag(xml, "ИспользоватьОбщиеДополнительныеРеквизитыИСведения", String(ini));
	//if ini = false then
	// return wap.doneResponse(xml);
	//endif;	
	
	attribsStore = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения;
	// Проверка на доп.реквизит OID
	oid = attribsStore.НайтиПоРеквизиту("Заголовок","OID");
	//if oid.IsEmpty() = false then
	// 	oid.ПолучитьОбъект().Удалить();
	//endif;
	if setup = true then
		if oid.IsEmpty() then
  		oid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.СоздатьЭлемент();
		else
			oid = oid.ПолучитьОбъект();
		endif;
  	oid.Заголовок = "OID";
  	oid.Наименование = "OID";
  	oid.ТипЗначения =  new ОписаниеТипов("Строка", , new StringQualifiers(36));
		oid.ЭтоДополнительноеСведение = true;
  	oid.Записать();
  	oid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Заголовок","OID");
  endif;
  wap.writeTag(xml, "OID", String(NOT oid.IsEmpty()));
  // Проверка на доп.реквизит ProcOID
	poid = attribsStore.НайтиПоРеквизиту("Заголовок","ProcOID");
	if setup = true then
		if poid.IsEmpty() then
  		poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.СоздатьЭлемент();
		else
			poid = poid.ПолучитьОбъект();
		endif;
  	poid.Заголовок = "ProcOID";
  	poid.Наименование = "ProcOID";
  	poid.ТипЗначения =  new ОписаниеТипов("Строка", , new StringQualifiers(36));
		poid.ЭтоДополнительноеСведение = true;
  	poid.Записать();
  	poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Заголовок","ProcOID");
  endif;
  wap.writeTag(xml, "ProcOID", String(NOT poid.IsEmpty()));
	// Проверка на доп.реквизит ProcType
	poid = attribsStore.НайтиПоРеквизиту("Заголовок","ProcType");
	if setup = true then
		if poid.IsEmpty() then
  		poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.СоздатьЭлемент();
		else
			poid = poid.ПолучитьОбъект();
		endif;
  	poid.Заголовок = "ProcType";
  	poid.Наименование = "ProcType";
  	poid.ТипЗначения =  new ОписаниеТипов("Строка");
		poid.ЭтоДополнительноеСведение = true;
  	poid.Записать();
  	poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Заголовок","ProcType");
  endif;
  wap.writeTag(xml, "ProcType", String(NOT poid.IsEmpty()));
	// Проверка на доп.реквизит ProcNumber
	poid = attribsStore.НайтиПоРеквизиту("Заголовок","ProcNumber");
	if setup = true then
		if poid.IsEmpty() then
  		poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.СоздатьЭлемент();
		else
			poid = poid.ПолучитьОбъект();
		endif;
  	poid.Заголовок = "ProcNumber";
  	poid.Наименование = "ProcNumber";
  	poid.ТипЗначения = new ОписаниеТипов("Строка");
		poid.ЭтоДополнительноеСведение = true;
  	poid.Записать();
  	poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Заголовок","ProcNumber");
  endif;
  wap.writeTag(xml, "ProcNumber", String(NOT poid.IsEmpty()));
	// Проверка на доп.реквизит ImportDate
	poid = attribsStore.НайтиПоРеквизиту("Заголовок","ImportDate");
	if setup = true then
		if poid.IsEmpty() then
  		poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.СоздатьЭлемент();
		else
			poid = poid.ПолучитьОбъект();
		endif;
  	poid.Заголовок = "ImportDate";
  	poid.Наименование = "ImportDate";
  	poid.ТипЗначения = new ОписаниеТипов("Строка");
		poid.ЭтоДополнительноеСведение = true;
  	poid.Записать();
  	poid = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Заголовок","ImportDate");
  endif;
  wap.writeTag(xml, "ImportDate", String(NOT poid.IsEmpty()));


	
	return wap.doneResponse(xml);
except	
	return wap.exceptionResponse(ErrorInfo());
endTry;	
EndFunction

/// Список плана счетов
function accountsPlanGET(Request)
try
	xml = wap.newXmlResponse();
	
  plan = ПланыСчетов.Хозрасчетный;

	select = plan.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЗапретитьИспользоватьВПроводках = true then
			continue;
		endif;

		xml.WriteStartElement("account");
		xml.WriteAttribute("code",  select.Код);
		xml.WriteAttribute("kind",  String(select.Вид));
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); // account
	enddo;                              
		
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;

endFunction


