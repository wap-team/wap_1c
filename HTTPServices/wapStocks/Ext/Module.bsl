﻿//*** Товары ***//
function opIndexGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "index" then
		select = Справочники.Номенклатура.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
				continue;
			endif;
			xml.WriteStartElement("Stock");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
    	//xml.WriteAttribute("id",  select.Код);
			xml.WriteAttribute("kind", String(select.ВидНоменклатуры));
			xml.WriteText(select.Наименование);
			xml.WriteEndElement(); 	
		enddo;    
	elsif oid = "list" then
		select = Справочники.Номенклатура.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true  or select.ЭтоГруппа = true then
				continue;
			endif;
			stockToXml(select, xml);
		enddo;                              
	else
		oid = new UUID(oid);
		ref = Справочники.Номенклатура.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		if ref.IsFolder = true then
			return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
		endif;
		stockToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function opIndexPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Stock" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createOrUpdateStock(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function opIndexPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Index" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createOrUpdateStock(xmlIter, resultXml);
		// пропуск нужен будет при ошибке или уже существует, если будут многотеговые Stock
		//if xmlIter.tag <> undefined then 
		//	while wap.iterNextXmlElement(xmlIter, "Stock") do
		//	enddo;
		//endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function opIndexDELETE(Request)
	return wap.doDeleteByXml(request, Справочники.Номенклатура, "Stock");
endFunction
procedure createOrUpdateStock(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Stock");
  if iter.tag <> "Stock" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	
try	
	oid = new UUID(iter.attributes.oid);
	objRef = Справочники.Номенклатура.ПолучитьСсылку(oid);
	isNew = wap.isEmptyRef(objRef);
	if isNew = true then
		obj = Справочники.Номенклатура.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(objRef); 
	else
		obj = objRef.GetObject();
	endif;	
	
	if iter.attributes.Property("id") = true then
		obj.Код = iter.attributes.id;
	endif;
	if iter.attributes.Property("type") = true then
		select = Справочники.ВидыНоменклатуры.Выбрать();
		kind = undefined;
		while select.next()	do
			if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
				continue;
			endif;
			if  iter.attributes.type = select.Наименование then
				kind = select.ref;
				break;
			endif;
		enddo; 
		if wap.isEmptyRef(kind) = true then
			raise "Указанный вид номенклатуры [" + iter.attributes.type + "] не существует!";
		else
			obj.ВидНоменклатуры = kind;
			obj.Услуга = kind.Услуга;
		endif;
	endif;
	
	if wap.isEmptyString(iter.text) = false then
		obj.Наименование = iter.text;
	else 
		while wap.iterNextXmlElement(iter,"Stock") do
			if iter.tag = "ID" then
				obj.Код = iter.text;
			//elsif iter.tag = "Name" then
				obj.Наименование = iter.text;
			elsif iter.tag = "Sku" then
				obj.Артикул = iter.text;
			//elsif iter.tag = "IsService" then
			//	obj.Услуга = Boolean(iter.text);
			elsif iter.tag = "StockGroup" then
				oid = new UUID(iter.text);
				group = Справочники.Номенклатура.ПолучитьСсылку(oid);
				if wap.isEmptyRef(group) = true and wap.isEmptyUUID(oid) = false then
					raise "Указанная группа номенклатуры не существует.";
				endif;
				obj.Родитель = group;
			elsif iter.tag = "StockKind" then
				oid = new UUID(iter.text);
				kind = Справочники.ВидыНоменклатуры.ПолучитьСсылку(oid);
				if wap.isEmptyRef(kind) = true and wap.isEmptyUUID(oid) = false then
					raise "Указанный вид номенклатуры не существует.";
				endif;
				obj.ВидНоменклатуры = kind;
				obj.Услуга = kind.Услуга;
			elsif iter.tag = "MeasureUnit" then
				unit = Справочники.КлассификаторЕдиницИзмерения.НайтиПоКоду(iter.attributes.code);
				if iter.attributes.code <> "0" and wap.isEmptyRef(unit) = true then
					unit = Справочники.КлассификаторЕдиницИзмерения.СоздатьЭлемент();
					unit.Код = iter.attributes.code;
					unit.Наименование = iter.text;
					unit.НаименованиеПолное = iter.text;
					unit.Write();
					unit = unit.ref;
				endif;
				obj.ЕдиницаИзмерения = unit;
			endif;
		enddo;
  endif;

	obj.Write();
	
	wap.setExtraAttribute(objRef, "ImportDate",  String(ТекущаяДата()));
	
	resultXml.WriteText(?(isNew, "создан", "обновлен"));
	resultXml.WriteEndElement(); // Stock
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Stock
endtry;
endprocedure
procedure stockToXml(obj, xml)
	xml.WriteStartElement("Stock");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	
	wap.writeTag(xml, "ID",  obj.Код);
	wap.writeTag(xml, "Name", obj.Наименование);
	wap.writeTag(xml, "Sku", obj.Артикул);
	//wap.writeTag(xml, "IsService", String(obj.Услуга));
	
	//wap.writeTag(xml, "FullName", obj.НаименованиеПолное);
	wap.writeTag(xml, "StockGroup", String(obj.Родитель.ref.UUID()));
	wap.writeTag(xml, "StockKind", String(obj.ВидНоменклатуры.ref.UUID()));
	wap.writeTag(xml, "MeasureUnit", obj.ЕдиницаИзмерения.Код);
	
	xml.WriteEndElement(); 	
endprocedure


//==== Old ====


//*** units - единицы измерения количества товара ***//
function unitsGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.КлассификаторЕдиницИзмерения.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true then
			continue;
		endif;
		xml.WriteStartElement("MeasureUnit");
		xml.WriteAttribute("code", select.Код);
		xml.WriteAttribute("notation", select.Наименование);
		xml.WriteAttribute("name", select.НаименованиеПолное);
		xml.WriteEndElement(); // MeasureUnit
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction
function unitsPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;

	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	
	codes = new Map();
	select = Справочники.КлассификаторЕдиницИзмерения.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true then
			continue;
		endif;
		codes.Insert(Number(select.Код), select.Ссылка);
	enddo; 
	
	while wap.iterNextXmlElement(iter) = true do
		if iter.tag <> "MeasureUnit" then
			return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
		endif;
		code = codes.Get(Number(iter.attributes.code));
		if code = undefined then
			code = Справочники.КлассификаторЕдиницИзмерения.СоздатьЭлемент();
			code.Код = ?(StrLen(iter.attributes.code) < 3, "0" , "") + iter.attributes.code;
		else
			code = code.ПолучитьОбъект();
			codes.Delete(Number(iter.attributes.code));
		endif;
		code.Наименование = iter.attributes.notation;
		code.НаименованиеПолное = iter.attributes.name;
		code.Write();
	enddo;
	
	for each doDel in codes do
		code = doDel.Value.ПолучитьОбъект();
		code.ПометкаУдаления = true;
		code.Write();
	enddo;
	
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

//*** Виды номенклатуры ***//
function kindsGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "list" then
		select = Справочники.ВидыНоменклатуры.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
		  xml.WriteStartElement("StockKind");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			xml.WriteAttribute("isService",  String(select.Услуга));
			xml.WriteText(select.Наименование);
			xml.WriteEndElement(); // StockKind
		enddo;                              
	else
		oid = new UUID(oid);
		select = Справочники.ВидыНоменклатуры.ПолучитьСсылку(oid);
		if wap.isEmptyRef(select) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
		endif;
		xml.WriteStartElement("StockKind");
		xml.WriteAttribute("oid",  String(select.ref.UUID()));
		xml.WriteAttribute("isService",  String(select.Услуга));
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); // StockKind
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction
function kindsPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.ВидыНоменклатуры.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		obj = Справочники.ВидыНоменклатуры.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
		obj.Write();
  endif;
	
	return kindsPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function kindsPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.ВидыНоменклатуры.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "StockKind" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	obj = ref.GetObject();
	obj.Наименование = iter.text;
	obj.Услуга = iter.attributes.isService = "Yes";
	obj.Write();
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function kindsDELETE(request)
	return wap.doDeleteByXml(request,  Справочники.ВидыНоменклатуры, "StockKind");
endFunction

//*** groups - дерево номенклатуры ***//
function groupsGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "list" then
		select = Справочники.Номенклатура.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true or select.ЭтоГруппа = false then
				continue;
			endif;
			xml.WriteStartElement("StockGroup");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			//xml.WriteAttribute("code",  String(select.Код));
			xml.WriteAttribute("pid",  String(select.Parent.ref.UUID()));
			xml.WriteAttribute("kind",  String(select.ВидНоменклатуры.ref.UUID()));
			xml.WriteText(select.Наименование);
			xml.WriteEndElement(); // StockTree
		enddo;                              
	else
		oid = new UUID(oid);
		ref = Справочники.Номенклатура.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
		endif;
			xml.WriteStartElement("StockGroup");
			xml.WriteAttribute("oid",  String(ref.ref.UUID()));
			xml.WriteAttribute("pid",  String(ref.Parent.ref.UUID()));
			xml.WriteAttribute("kind",  String(ref.ВидНоменклатуры.ref.UUID()));
			xml.WriteText(ref.Наименование);
			xml.WriteEndElement(); // StockTree
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction
function groupsPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Номенклатура.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		obj = Справочники.Номенклатура.СоздатьГруппу();
		obj.УстановитьСсылкуНового(ref); 
		obj.Write();
  endif;
	
	return groupsPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function groupsPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Номенклатура.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
	endif;
	
	if ref.ЭтоГруппа = false then
		return wap.errorResponse(404, "Элемент не является группой номенклатуры.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "StockGroup" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	obj = ref.GetObject();
	obj.Наименование = iter.text;
	obj.Parent = Справочники.Номенклатура.ПолучитьСсылку(new UUID(iter.attributes.pid));
	obj.ВидНоменклатуры = Справочники.ВидыНоменклатуры.ПолучитьСсылку(new UUID(iter.attributes.kind));
	obj.Write();
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function groupsDELETE(request)
	return wap.doDeleteByXml(request, Справочники.Номенклатура, "StockGroup");
endFunction

