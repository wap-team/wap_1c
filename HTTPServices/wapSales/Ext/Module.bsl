﻿//*** Продажи - Реализация товаров и услуг в розницу  ***//
function retailGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.РеализацияТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			// !!!!
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
			// !!!
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;   
	elsif oid = "/list" then
		select = Документы.РеализацияТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			retailToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		retailToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function retailPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createRetail(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function retailPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createRetail(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function retailDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createRetail(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	// !!!	
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	// !!!
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.РеализацияТоваровУслуг.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		// !!!
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		// !!!
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	

	//doc.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	// !!!
	//doc.Организация = wap.getCurrentUserOrganization();
	doc.СпособЗачетаАвансов = Перечисления.СпособыЗачетаАвансов.Автоматически;
	doc.СчетУчетаРасчетовСКонтрагентом = ПланыСчетов.Хозрасчетный.РасчетыСПокупателями; //62.01 - по умолчанию
	doc.СчетУчетаРасчетовПоАвансам = ПланыСчетов.Хозрасчетный.РасчетыПоАвансамВыданным; //60.02 - по умолчанию
	
	writeMode = undefined;
	stocksCount = 0;
	servicesCount = 0;
	item = undefined;
	itemDebit = "90.01.1";
	itemCredit = "90.02.1";
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийРеализацияТоваров, iter.text);
		// !!!!!	
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			doc.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
		elsif iter.tag = "Debit" then	
			itemDebit = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.КассаОрганизации;
		elsif iter.tag = "Credit" then	
			itemCredit = wap.accountFromString(iter.text); 
			//doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
	
			
		elsif iter.tag = "Item" then
			if iter.attributes.Property("oid") = false then
				raise("Ошибка формата XML запроса: oid товара не указан!");
			endif;
			stockRef = Справочники.Номенклатура.ПолучитьСсылку(new UUID(iter.attributes.oid));
			if wap.isEmptyRef(stockRef) then
				raise "Товар с OID = [" + iter.attributes.oid + "] не найден!";
 			endif;
			if stockRef.IsFolder = true then
				raise "Товар с OID = [" + iter.attributes.oid + "] является папкой!";
			endif;
			
			if stockRef.Услуга = true then
				servicesCount = servicesCount + 1;
				item = doc.Услуги.Добавить();
			else
				stocksCount = stocksCount + 1;
				item = doc.Товары.Добавить();
				item.СчетУчета = wap.accountFromString("41.01");
			endif;
			item.Номенклатура = stockRef;
			item.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			item.Количество = 1;
			item.СчетДоходов = wap.accountFromString(itemDebit);
			item.СчетРасходов = wap.accountFromString(itemCredit);
		elsif iter.tag = "Amount" then
      item.Количество = Number(iter.text);
		elsif iter.tag = "Summa" then
      item.Цена = Number(iter.text);
			item.Сумма = Number(iter.text);
		elsif iter.tag = "NdsRate" then
      item.СтавкаНДС =  wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		endif;
	enddo;
	
	if ЗначениеЗаполнено(doc.ВидОперации) = false then
		if stocksCount > 0 and servicesCount > 0 then
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.ПродажаКомиссия;
		elsif servicesCount > 0 then
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.Услуги;
		else
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.Товары;
		endif;
	endif;
	
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	// !!!
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	// !!!
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure retailToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	// !!!
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
	// !!!
	
	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	elsif	obj.ПринятоОт <> "" then
		wap.writeTag(xml, "Payer", obj.ПринятоОт);
 	endif; 
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	wap.writeTag(xml, "Note", obj.Комментарий);
	
	xml.WriteStartElement("Items");
	for each item in obj.Товары do
		xml.WriteStartElement("Item");
		xml.WriteAttribute("oid",  String(item.Номенклатура.ref.UUID()));
		wap.writeTag(xml,"Amount", item.Количество);
		wap.writeTag(xml,"Price", item.Цена);
		wap.writeTag(xml,"Summa", item.Сумма);
		wap.writeTag(xml,"NDS", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	for each item in obj.Услуги do
		xml.WriteStartElement("Item");
		xml.WriteAttribute("oid",  String(item.Номенклатура.ref.UUID()));
		wap.writeTag(xml,"Amount", item.Количество);
		wap.writeTag(xml,"Price", item.Цена);
		wap.writeTag(xml,"Summa", item.Сумма);
		wap.writeTag(xml,"NDS", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure

//*** Продажи - Реализация слуг по договору ***//
function leaseGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.РеализацияТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> "ProcessLeasePayment" then
				continue;
			endif;
			
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;   
	elsif oid = "/list" then
		select = Документы.РеализацияТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			if wap.getProcType(select.ref) <> "ProcessLeasePayment" then
				continue;
			endif;
			leaseToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		leaseToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function leasePUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createLease(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function leasePOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createLease(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function leaseDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createLease(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
	isUpdate = false;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.РеализацияТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.РеализацияТоваровУслуг.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		//wap.writeTag(resultXml, "Number",  docRef.Номер);
    //wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		//wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		//resultXml.WriteEndElement(); // Doc
		//return;
		isUpdate = true;
		doc = docRef.ПолучитьОбъект();
		if doc.Проведен = true then
    	doc.Записать(РежимЗаписиДокумента.ОтменаПроведения);
		endif;
		doc.Услуги.Очистить();
		doc.Товары.Очистить();
	endif;	

	//doc.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	doc.СпособЗачетаАвансов = Перечисления.СпособыЗачетаАвансов.Автоматически;
	doc.СчетУчетаРасчетовСКонтрагентом = ПланыСчетов.Хозрасчетный.РасчетыСПокупателями; //62.01 - по умолчанию
	doc.СчетУчетаРасчетовПоАвансам = ПланыСчетов.Хозрасчетный.РасчетыПоАвансамВыданным; //60.02 - по умолчанию
	
	procOID = undefined;
	writeMode = undefined;
	stocksCount = 0;
	servicesCount = 0;
	item = undefined;
	itemDebit = "90.01.1";
	itemCredit = "90.02.1";
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийРеализацияТоваров, iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);		
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			doc.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
		elsif iter.tag = "Debit" then	
			itemDebit = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.КассаОрганизации;
		elsif iter.tag = "Credit" then	
			itemCredit = wap.accountFromString(iter.text); 
			//doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
	
			
		elsif iter.tag = "Item" then
			if iter.attributes.Property("oid") = false then
				raise("Ошибка формата XML запроса: oid товара не указан!");
			endif;
			stockRef = Справочники.Номенклатура.ПолучитьСсылку(new UUID(iter.attributes.oid));
			if wap.isEmptyRef(stockRef) then
				raise "Товар с OID = [" + iter.attributes.oid + "] не найден!";
 			endif;
			if stockRef.IsFolder = true then
				raise "Товар с OID = [" + iter.attributes.oid + "] является папкой!";
			endif;
			
			if stockRef.Услуга = true then
				servicesCount = servicesCount + 1;
				item = doc.Услуги.Добавить();
			else
				stocksCount = stocksCount + 1;
				item = doc.Товары.Добавить();
				item.СчетУчета = wap.accountFromString("41.01");
			endif;
			item.Номенклатура = stockRef;
			item.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			item.Количество = 1;
			item.СчетДоходов = wap.accountFromString(itemDebit);
			item.СчетРасходов = wap.accountFromString(itemCredit);
		elsif iter.tag = "Amount" then
      item.Количество = Number(iter.text);
		elsif iter.tag = "Summa" then
      item.Цена = Number(iter.text);
			item.Сумма = Number(iter.text);
		elsif iter.tag = "NdsRate" then
      item.СтавкаНДС =  wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		endif;
	enddo;
	
	if ЗначениеЗаполнено(doc.ВидОперации) = false or isUpdate = true then
		if stocksCount > 0 and servicesCount > 0 then
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.ПродажаКомиссия;
		elsif servicesCount > 0 then
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.Услуги;
		else
			doc.ВидОперации = Перечисления.ВидыОперацийРеализацияТоваров.Товары;
		endif;
	endif;
	
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = ?(isUpdate, "обновлен, записан", "только записан");
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = ?(isUpdate, "обновлен, проведен", "записан, проведен");
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure leaseToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;

	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	elsif	obj.ПринятоОт <> "" then
		wap.writeTag(xml, "Payer", obj.ПринятоОт);
 	endif; 
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	wap.writeTag(xml, "Note", obj.Комментарий);
	
	xml.WriteStartElement("Items");
	for each item in obj.Товары do
		xml.WriteStartElement("Item");
		xml.WriteAttribute("oid",  String(item.Номенклатура.ref.UUID()));
		wap.writeTag(xml,"Amount", item.Количество);
		wap.writeTag(xml,"Price", item.Цена);
		wap.writeTag(xml,"Summa", item.Сумма);
		wap.writeTag(xml,"NDS", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	for each item in obj.Услуги do
		xml.WriteStartElement("Item");
		xml.WriteAttribute("oid",  String(item.Номенклатура.ref.UUID()));
		wap.writeTag(xml,"Amount", item.Количество);
		wap.writeTag(xml,"Price", item.Цена);
		wap.writeTag(xml,"Summa", item.Сумма);
		wap.writeTag(xml,"NDS", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure
