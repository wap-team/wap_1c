﻿//*** Приходный кассовый ордер ***//
function pkoGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.ПриходныйКассовыйОрдер.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.ПриходныйКассовыйОрдер.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			pkoToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.ПриходныйКассовыйОрдер.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		pkoToXml(ref, xml);
	endif;
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function pkoPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createPko(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function pkoPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createPko(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function pkoDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.ПриходныйКассовыйОрдер.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createPko(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.ПриходныйКассовыйОрдер.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.ПриходныйКассовыйОрдер.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	

	doc.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	
	procNumber = undefined;
	item = undefined;
	writeMode = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "ProcOID" then
			procOID = new UUID(iter.text);
		elsif iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "ProcNumber" then
     procNumber = iter.text;
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийПКО, iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Reason" then
			doc.Основание = iter.text;
		elsif iter.tag = "NdsRate" then
			doc.СтавкаНДС = wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			doc.ПринятоОт = doc.Контрагент.Наименование;
		elsif iter.tag = "Summa_Cash" or iter.tag = "Summa_Card" then
			if doc.Контрагент = undefined then
				raise "Контрагент не определен к моменту определения договора!";
			endif;
			item = doc.РасшифровкаПлатежа.Добавить();
			item.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
			item.СуммаПлатежа = Number(iter.text);
			item.КурсВзаиморасчетов = 1;
			item.СуммаВзаиморасчетов = item.СуммаПлатежа;
			item.СтавкаНДС = doc.СтавкаНДС;
			item.СпособПогашенияЗадолженности = Перечисления.СпособыПогашенияЗадолженности.Автоматически;
			item.КратностьВзаиморасчетов = 1;
			item.ПорядокОтраженияАванса = Перечисления.ПорядокОтраженияАвансов.ДоходУСН;
			item.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString("62.01");
 			item.СчетУчетаРасчетовПоАвансам = wap.accountFromString("62.02");
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
		elsif iter.tag = "Debit" then	
			doc.СчетКасса = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.КассаОрганизации;
		elsif iter.tag = "Credit" then	
			doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.
			if item <> undefined then
				item.СчетУчетаРасчетовСКонтрагентом  =  wap.accountFromString(iter.text);
			endif;
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
	endif;
	enddo;
	
	if ЗначениеЗаполнено(doc.ВидОперации) = false then
		raise("Не указан вид операции!");
	endif;
	
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	if procNumber <> undefined then
		wap.setProcNumber(docRef, procNumber);
	endif;
	wap.setExtraAttribute(docRef, "ImportDate",  String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure pkoToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
		
	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	elsif	obj.ПринятоОт <> "" then
		wap.writeTag(xml, "Payer", obj.ПринятоОт);
 	endif; 
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	wap.writeTag(xml, "Note", obj.Комментарий);
	
	xml.WriteStartElement("Items");
	for each item in obj.РасшифровкаПлатежа do
		xml.WriteStartElement("Item");
		wap.writeTag(xml,"Summa", item.СуммаПлатежа);
		wap.writeTag(xml,"NdsRate", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure

//*** Расходный кассовый ордер ***//
function rkoGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.РасходныйКассовыйОрдер.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			procType = wap.getProcType(select.ref);
			if procType <> undefined then
				xml.WriteAttribute("proc_type",  procType);
			endif;
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;     
	elsif oid = "/list" then
		select = Документы.РасходныйКассовыйОрдер.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			rkoToXml(select, xml);
		enddo;                              
	else                                   
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.РасходныйКассовыйОрдер.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		rkoToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function rkoPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	createRko(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function rkoPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	createRko(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function rkoDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.РасходныйКассовыйОрдер.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createRko(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.РасходныйКассовыйОрдер.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.РасходныйКассовыйОрдер.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	
	
	doc.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	
	procNumber = undefined;
	item = undefined;
	writeMode = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Number" then
      doc.Номер =  iter.text;
		elsif iter.tag = "ProcNumber" then
    	procNumber = iter.text;	
		elsif iter.tag = "OperKind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийРКО, iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Reason" then
			doc.Основание = iter.text;
		elsif iter.tag = "NdsRate" then
			doc.СтавкаНДС = wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			doc.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПокупателем);
			item = doc.РасшифровкаПлатежа.Добавить();
			item.ДоговорКонтрагента = doc.ДоговорКонтрагента;
			item.СпособПогашенияЗадолженности = Перечисления.СпособыПогашенияЗадолженности.Автоматически;
			item.СтавкаНДС = doc.СтавкаНДС;
			item.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString("62.01");
 			item.СчетУчетаРасчетовПоАвансам = wap.accountFromString("62.02");
			item.КурсВзаиморасчетов = 1;
			item.КратностьВзаиморасчетов = 1;
			item.СуммаВзаиморасчетов = item.СуммаПлатежа;
			item.СтавкаНДС = doc.СтавкаНДС;
		elsif iter.tag = "Employee" then	
			emplOID = new UUID(iter.text);
			select = Справочники.Сотрудники.Выбрать();
			empl = undefined;
			while select.next()
			do
				if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
					continue;
				endif;
				if select.ФизическоеЛицо.UUID() = emplOID then
					empl = select.ФизическоеЛицо;
					break;
				endif;
			enddo;		
			if empl = undefined then
				raise "Не найден сотрудник с OID = [" + iter.text + "]!";
			endif;
			doc.Контрагент = empl;
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
			if item <> undefined then
				item.СуммаПлатежа = Number(iter.text);
				item.СуммаВзаиморасчетов = item.СуммаПлатежа;
			endif;
		elsif iter.tag = "Debit" then	
			doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.
			if item <> undefined then
				item.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text);
			endif;
		elsif iter.tag = "Credit" then	
			doc.СчетКасса = wap.accountFromString(iter.text); 
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
	endif;
	
	enddo;
	
	if ЗначениеЗаполнено(doc.ВидОперации) = false then
		raise("Не указан вид операции!");
	endif;
	
	
	//doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	//check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	if procNumber <> undefined then
		wap.setProcNumber(docRef, procNumber);
	endif;
	wap.setExtraAttribute(docRef, "ImportDate",  String(ТекущаяДата()));

	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure rkoToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;
	
	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "OperKind", wap.enumToString(obj.ВидОперации));
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
 	endif; 
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	wap.writeTag(xml, "Note", obj.Комментарий);
	
	xml.WriteStartElement("Items");
	for each item in obj.РасшифровкаПлатежа do
		xml.WriteStartElement("Item");
		wap.writeTag(xml,"Summa", item.СуммаПлатежа);
		wap.writeTag(xml,"NdsRate", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Item
	enddo;	
	xml.WriteEndElement(); // Items
	
	xml.WriteEndElement(); // Doc	
endprocedure


// ??? *** Розничная продажа (чек) ***//
function checkGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/list" then
		select = Документы.РозничнаяПродажа.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			checkToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.РозничнаяПродажа.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		checkToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function checkPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Create" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Doc" then
		return wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	//if iter.attributes.oid <> Request.URLParameters["oid"] then
	//	return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	//endif;
	
	doc = Документы.РозничнаяПродажа.СоздатьДокумент();
	doc.СуммаВключаетНДС = true;
	doc.ВидОперации = Перечисления.ВидыОперацийРозничнаяПродажа.Продажа;
	doc.Организация = wap.default("Организация");
	doc.Склад = wap.default("Склад");
	doc.Дата = ТекущаяДата();
	doc.УстановитьВремя();
	
	item = undefined;
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Kind" then
			doc.ВидОперации = wap.enumFromString(Перечисления.ВидыОперацийРозничнаяПродажа, iter.text);
		elsif iter.tag = "KKMCheckNumber" then
			doc.НомерЧекаККМ = iter.text;
		elsif iter.tag = "NoNDS" then
			doc.ДокументБезНДС = iter.text = "Yes";
		elsif iter.tag = "TotalWithNDS" then
			doc.СуммаВключаетНДС = iter.text = "Yes";
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
			doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
			if wap.isEmptyRef(doc.ВалютаДокумента) then
				raise "Не найдена валюта Рубль!";
			endif;
		elsif iter.tag = "Comment" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Stock" then	
			oid = new UUID(iter.attributes.oid);
			ref = Справочники.Номенклатура.ПолучитьСсылку(oid);
			if wap.isEmptyRef(ref) or ref.IsFolder = true then
				return wap.errorResponse(404, "Позиция номенклатуры с указанным OID не найдена.");
			endif;
			item = doc.Товары.Добавить();
			item.Номенклатура = ref;
		elsif iter.tag = "Amount" then
			item.Количество = Number(iter.text);
		elsif iter.tag = "Price" then
			item.Цена = Number(iter.text);
		elsif iter.tag = "Summa" then
			item.Сумма = Number(iter.text);
		elsif iter.tag = "NDS" then
			item.СтавкаНДС = wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "SummaNDS" then
			item.СуммаНДС = Number(iter.text);

		elsif iter.tag = "Сertificate" then
			item = doc.ПодарочныеСертификаты.Добавить();
			item.ВидОплаты = wap.default("ВидОплатСертификат");
	//elsif iter.tag = "Summa" then
	//	item.Сумма = Number(iter.text);
			
		elsif iter.tag = "Payment" then
			item = doc.Оплата.Добавить();
			item.ВидОплаты = wap.convertPaymentKind(iter.attributes.kind);
	//elsif iter.tag = "Summa" then
	//	item.Сумма = Number(iter.text);
		elsif iter.tag = "TerminalCheckNumber" then
			item.НомерЧекаЭТ = iter.text;
		elsif iter.tag = "CardNumber" then
			item.НомерПлатежнойКарты = iter.text;
		endif;
	enddo;
	
	doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	if doc.ПроверитьЗаполнение() = false then
		raise "Не пройдена проверка заполнения документа!";
	endif;	
	doc.Write(РежимЗаписиДокумента.Проведение);
	
	xml = wap.newXmlResponse("Result");
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(doc.ref.UUID()));
		wap.writeTag(xml, "Number", doc.Номер);
		wap.writeTag(xml, "DocType", "РозничнаяПродажа");
		wap.writeTag(xml, "DocName", String(doc));
	xml.WriteEndElement(); // Doc
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function checkDELETE(Request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.РозничнаяПродажа.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure checkToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	
	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	wap.writeTag(xml, "Kind", obj.ВидОперации);
	wap.writeTag(xml, "KKMCheckNumber", obj.НомерЧекаККМ);
	wap.writeTag(xml, "NoNDS", obj.ДокументБезНДС);
	wap.writeTag(xml, "TotalWithNDS", obj.СуммаВключаетНДС);
	wap.writeTag(xml, "DocTotal", wap.moneyToString(obj.СуммаДокумента));
	wap.writeTag(xml, "Comment", obj.Комментарий);
	
	/// Товары / услуги
	xml.WriteStartElement("Stocks");
	
	for each item in obj.Товары do
		xml.WriteStartElement("Stock");
		xml.WriteAttribute("oid",  String(item.Номенклатура.ref.UUID()));
			wap.writeTag(xml,"Amount", item.Количество);
			wap.writeTag(xml,"Price", wap.moneyToString(item.Цена));
			wap.writeTag(xml,"Summa",  wap.moneyToString(item.Сумма));
			wap.writeTag(xml,"NDS", item.СтавкаНДС);
			wap.writeTag(xml,"SummaNDS",  wap.moneyToString(item.СуммаНДС));
		xml.WriteEndElement(); // Stock
	enddo;	
	
	/// Продажи сертификатов
	for each item in obj.ПодарочныеСертификаты do
		xml.WriteStartElement("Сertificate");
		//xml.WriteAttribute("kind",  wap.convertPaymentKind(item.ВидОплаты.ref));
			wap.writeTag(xml,"Summa",  wap.moneyToString(item.Сумма));
		xml.WriteEndElement(); // CertSale
	enddo;
	
	xml.WriteEndElement(); // Stocks
	
	/// Безналичные оплаты
	if obj.Оплата.Количество() > 0 then
		xml.WriteStartElement("Payments");
		for each item in obj.Оплата do
			xml.WriteStartElement("Payment");
			xml.WriteAttribute("kind", wap.convertPaymentKind(item.ВидОплаты.ref));
				wap.writeTag(xml,"Summa",  wap.moneyToString(item.Сумма));
				wap.writeTag(xml,"TerminalCheckNumber", item.НомерЧекаЭТ);
				wap.writeTag(xml,"CardNumber", item.НомерПлатежнойКарты);
			xml.WriteEndElement(); // Payment
		enddo;	
		xml.WriteEndElement(); // Payments
	endif;
	
	xml.WriteEndElement(); // Doc	
endprocedure

