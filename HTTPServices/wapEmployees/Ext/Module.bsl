﻿//*** Сотрудники ***//
function indexGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.Сотрудники.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
			continue;
		endif;
		
		xml.WriteStartElement("Employee");
		xml.WriteAttribute("oid",  String(select.ФизическоеЛицо.UUID()));
    //xml.WriteAttribute("id",  select.Код);
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); 	
		
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function indexPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Index" then
		return wap.errorResponse(422, "Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
		createOrUpdateEmployee(xmlIter, resultXml);
	enddo;		
	
	return wap.doneResponse(resultXml); //"<OK/>");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createOrUpdateEmployee(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Employee");
  if iter.tag <> "Employee" then
		raise("Ошибка формата XML запроса: " + iter.tag);
		endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;	
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	
try	
	oid = new UUID(iter.attributes.oid);
	persRef = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if persRef.IsFolder = true then
		raise("Элемент с указанным OID является папкой.");
	endif;
	isNew = wap.isEmptyRef(persRef);
	if isNew then
		pers = Справочники.ФизическиеЛица.СоздатьЭлемент();
		pers.УстановитьСсылкуНового(persRef); 
	else
		pers = persRef.GetObject();
  endif;
		pers.ФИО = iter.text;
		pers.Наименование = iter.text;
		pers.Write();
		wap.setExtraAttribute(persRef, "ImportDate",  String(ТекущаяДата()));
			
	ref = Справочники.Сотрудники.НайтиПоРеквизиту("ФизическоеЛицо", persRef);
	if wap.isEmptyRef(ref) then
		isNew = true;
		obj = Справочники.Сотрудники.СоздатьЭлемент();
		obj.Наименование = persRef.Наименование;
		obj.ФизическоеЛицо = persRef;
		org = Справочники.Организации.Выбрать();
		org.next();
		obj.ГоловнаяОрганизация = org.Ссылка;
		obj.Write();
		wap.setExtraAttribute(obj.ref, "ImportDate",  String(ТекущаяДата()));
  endif;

	resultXml.WriteText(?(isNew, "создан", "обновлен"));
	resultXml.WriteEndElement(); // Employee
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Employee
endtry;
endprocedure


//=== OLD =====

//*** Должности ***//
function postsGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "list" then
		select = Справочники.Должности.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			xml.WriteStartElement("Post");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			wap.writeTag(xml, "Name", select.Наименование);
			wap.writeTag(xml, "ShortName", select.НаименованиеКраткое);
			xml.WriteEndElement(); // Post
		enddo;                              
	else
		oid = new UUID(oid);
		ref = Справочники.Должности.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
		endif;
		xml.WriteStartElement("Post");
		xml.WriteAttribute("oid",  String(ref.ref.UUID()));
		wap.writeTag(xml, "Name", ref.Наименование);
		wap.writeTag(xml, "ShortName", ref.НаименованиеКраткое);
		xml.WriteEndElement();
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction
function postsPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Должности.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		obj = Справочники.Должности.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
		obj.Write();
  endif;
	
	return postsPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function postsPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Должности.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Post" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	obj = ref.GetObject();
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "Name" then
			obj.Наименование = iter.text;
		elsif iter.tag = "ShortName" then
			obj.НаименованиеКраткое = iter.text;
		endif;
	enddo;
	obj.Write();
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function postsDELETE(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	ref = Справочники.Должности.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Delete" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Post" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true, true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

//*** Сотрудники ***//
function opIndexGET(Request)
try
	args = Request.QueryOptions; // wap.requestArgs(Request); 
	data = new Map();
	if args.get("data") <> undefined then
		//args["data"] = ?(IsBlankString(args["data"]), "person", args["data"]);
		for each x in StrSplit(args["data"], ",", false) do
			data[x] = true;
		enddo;
	endif;
		
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["oid"];
	if oid = "list" then
		select = Справочники.Сотрудники.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true then
				continue;
			endif;
			employeeToXml(select, xml, data);
		enddo;                              
	else
		oid = new UUID(oid);
		pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
			if wap.isEmptyRef(pers) then
				return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  		endif;
			if pers.IsFolder = true then
				return wap.errorResponse(404, "Элемент с указанным OID является папкой.");
			endif;
		ref = Справочники.Сотрудники.НайтиПоРеквизиту("ФизическоеЛицо", pers);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
		endif;
		employeeToXml(ref, xml, data);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function opIndexPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if wap.isEmptyRef(pers) then
		return wap.errorResponse(404, "Персона с указанным OID не найдена.");
  endif;
	ref = Справочники.Сотрудники.НайтиПоРеквизиту("ФизическоеЛицо", pers);
	if wap.isEmptyRef(ref) then
		obj = Справочники.Сотрудники.СоздатьЭлемент();
		obj.Наименование = pers.Наименование;
		obj.ФизическоеЛицо = pers;
		org = Справочники.Организации.Выбрать();
		org.next();
		obj.ГоловнаяОрганизация = org.Ссылка;
		obj.Write();
  endif;
	
	return opIndexPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function opIndexPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if wap.isEmptyRef(pers) then
		return wap.errorResponse(404, "Персона с указанным OID не найдена.");
  endif;
	ref = Справочники.Сотрудники.НайтиПоРеквизиту("ФизическоеЛицо", pers);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Сотрудник с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Employee" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	doc = Документы.ПриемНаРаботу.НайтиПоРеквизиту("Сотрудник", ref);
	if wap.isEmptyRef(doc) then
		doc = Документы.ПриемНаРаботу.СоздатьДокумент();
		doc.Сотрудник = ref;
		doc.Организация = ref.ГоловнаяОрганизация;
		doc.СпособРасчетаАванса = Перечисления.СпособыРасчетаАванса.ПроцентомОтТарифа;
		doc.Аванс = 40;
	else 
		doc = doc.GetObject();
	endif;
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "Division" then
			divOid = new UUID(iter.text);
			divRef = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(divOid);
			if wap.isEmptyRef(divRef) then
				return wap.errorResponse(404, "Подразделение с указанным OID не найдено.");
  		endif;
			doc.Подразделение = divRef;
		elsif iter.tag = "Post" then
			postOid = new UUID(iter.text);
			postRef = Справочники.Должности.ПолучитьСсылку(postOid);
			if wap.isEmptyRef(postRef) then
				return wap.errorResponse(404, "Подразделение с указанным OID не найдено.");
  		endif;
			doc.Должность = postRef;
		elsif iter.tag = "HireDate" then
			doc.ДатаПриема = wap.dateFromISOString(iter.text);
		elsif iter.tag = "Comment" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "OrderNumber" then
			// doc.Номер = iter.text; // Вроде как менять смысла нет
		elsif iter.tag = "OrderDate" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "HireKind" then
			doc.ВидЗанятости = wap.enumFromString(Перечисления.ВидыЗанятости, iter.text);
		elsif iter.tag = "HireCondition" then
			doc.УсловияПриема = iter.text;
		endif;
	enddo;         
	doc.Write(РежимЗаписиДокумента.Проведение);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
function opIndexDELETE(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	pers = Справочники.ФизическиеЛица.ПолучитьСсылку(oid);
	if wap.isEmptyRef(pers) then
		return wap.errorResponse(404, "Персона с указанным OID не найдена.");
  endif;
	ref = Справочники.Сотрудники.НайтиПоРеквизиту("ФизическоеЛицо", pers);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Сотрудник с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Delete" or iter.hasChilds = false then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Employee" or iter.hasChilds = false then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	wap.iterNextXmlElement(iter);
	if iter.tag <> "DismissalDate" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	
	//ref.GetObject().УстановитьПометкуУдаления(true, true);
	doc = Документы.Увольнение.НайтиПоРеквизиту("Сотрудник", ref);
	if wap.isEmptyRef(doc) then
		doc = Документы.Увольнение.СоздатьДокумент();
		doc.Дата = CurrentDate();
		doc.Организация = ref.ГоловнаяОрганизация;
		doc.Сотрудник = ref;
		doc.ФизическоеЛицо = pers;
		doc.ОснованиеУвольнения = "Заявление по собственному желанию";
	else
		doc = doc.GetObject();
	endif;
	
	doc.ДатаУвольнения = wap.dateFromISOString(iter.text);
	doc.Write(РежимЗаписиДокумента.Проведение);
	
	// Это полностью скрывает сотрудника из списка в 1С
	//obj = ref.GetObject();
	//obj.ВАрхиве = true;
	//obj.Write();

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

//--- Tools ---
procedure employeeToXml(empl, xml, dataMap)
	xml.WriteStartElement("Employee");
	person = empl.ФизическоеЛицо;
	xml.WriteAttribute("oid",  String(person.ref.UUID()));
	
	if dataMap["1c"] = true then
		wap.writeTag(xml, "ID",  String(empl.Код));
		wap.writeTag(xml, "EmplOID",  String(empl.ref.UUID()));
		wap.writeTag(xml, "Name", empl.Наименование);
		wap.writeTag(xml, "InArch", empl.ВАрхиве);
	endif;
	
	if dataMap["employee"] = true then
		if IsBlankString(person.ИНН) = false then
			wap.writeTag(xml, "INN",  person.ИНН);
		endif;
		if IsBlankString(person.СтраховойНомерПФР) = false then
			wap.writeTag(xml, "SNILS",  person.СтраховойНомерПФР);
		endif;
	endif;
	
	if dataMap["employee"] = true or dataMap["1c"] = true then
		doc = Документы.ПриемНаРаботу.НайтиПоРеквизиту("Сотрудник", empl.ref);
		if wap.isEmptyRef(doc) = false then
			if dataMap["employee"] = true then
				wap.writeTag(xml, "Division",  String(doc.Подразделение.ref.UUID()));	
				wap.writeTag(xml, "Post",  String(doc.Должность.ref.UUID()));	
				wap.writeTag(xml, "HireDate",  wap.dateToISOString(doc.ДатаПриема));	
				wap.writeTag(xml, "Comment",  doc.Комментарий);	
			endif;
			if dataMap["1c"] = true then
				wap.writeTag(xml, "HireOrderNumber", doc.Номер);
				wap.writeTag(xml, "HireOrderDate",  wap.dateToISOString(doc.Дата));
				wap.writeTag(xml, "HireKind", String(doc.ВидЗанятости));
				wap.writeTag(xml, "HireCondition", doc.УсловияПриема);
			endif;
		endif;
	endif;
	if dataMap["employee"] = true or dataMap["1c"] = true then
		doc = Документы.Увольнение.НайтиПоРеквизиту("Сотрудник", empl.ref);
		if wap.isEmptyRef(doc) = false then
			if dataMap["employee"] = true then
				wap.writeTag(xml, "DismissalDate",  wap.dateToISOString(doc.ДатаУвольнения));	
			endif;
			if dataMap["1c"] = true then
				wap.writeTag(xml, "DismissalOrderNumber", doc.Номер);
				wap.writeTag(xml, "DismissalOrderDate",  wap.dateToISOString(doc.Дата));
				wap.writeTag(xml, "DismissalReason", doc.ОснованиеУвольнения);
			endif;
		endif;
	endif;
	xml.WriteEndElement(); 	
endprocedure

