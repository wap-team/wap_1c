﻿
function listGET(Request)
try
	args = Request.QueryOptions; // wap.requestArgs(Request); 
	data = new Map();
	if args.get("data") <> undefined then
		//args["data"] = ?(IsBlankString(args["data"]), "person", args["data"]);
		for each x in StrSplit(args["data"], ",", false) do
			data[x] = true;
		enddo;
	endif;
		
	xml = wap.newXmlResponse();
	
	div = Справочники.ПодразделенияОрганизаций.Выбрать();
	while div.next()
	do
		if div.ПометкаУдаления = true then
			continue;
		endif;
		//wap.writeTag(xml,"Division", pers.Наименование);
		divisionToXml(div, xml, data);
	enddo;                              

	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endfunction

function opIndexGET(Request) 
try
	args = Request.QueryOptions;
	data = new Map();
	if args.get("data") <> undefined then
		//args["data"] = ?(IsBlankString(args["data"]), "person", args["data"]);
		for each x in StrSplit(args["data"], ",", false) do
			data[x] = true;
		enddo;
	endif;
		
	xml = wap.newXmlResponse(); 
	
	oid = new UUID(Request.URLParameters["oid"]);
	div = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(oid);
	if wap.isEmptyRef(div) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	divisionToXml(div, xml, data);

	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;

endFunction

function opIndexPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	divRef = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(oid);
	if wap.isEmptyRef(divRef) then
		div = Справочники.ПодразделенияОрганизаций.СоздатьЭлемент();
		div.УстановитьСсылкуНового(divRef); 
		org = Справочники.Организации.Выбрать();
		org.next();
		div.Владелец = org.Ссылка;
		div.ГоловнаяОрганизация = org.Ссылка;
		div.Write();
  endif;
	
	return opIndexPATCH(Request);
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

function opIndexPATCH(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	div = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(oid);
	if wap.isEmptyRef(div) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Update" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Division" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	if iter.attributes.Property("pid") = false or iter.attributes.pid = undefined then
		return wap.errorResponse(422, "Не указан OID родительского подразделения!");
	endif;
	pid = new UUID(iter.attributes.pid);
	
	div = div.GetObject();
	if pid <> div.Родитель.ref.UUID() then
		pdiv = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(pid);
		if wap.isEmptyRef(pdiv) and iter.attributes.pid <> "00000000-0000-0000-0000-000000000000" then
			return wap.errorResponse(404, "Не найдено родительское подразделение с указанным OID.");
		endif;
		div.Родитель = pdiv;
	endif;
	while wap.iterNextXmlElement(iter) do
		if iter.tag = "Name" then
			div.Наименование = iter.text;
		//elsif iter.tag = "FullName" then
		//	div.НаименованиеПолное = iter.text;
		endif;
	enddo;
	div.Write();
	
	wap.setExtraAttribute(div.ref, "ImportDate",  String(ТекущаяДата()));
	
	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction

Function opIndexDELETE(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(Request.URLParameters["oid"]);
	div = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(oid);
	if wap.isEmptyRef(div) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Delete" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> "Division" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> Request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	//div = div.GetObject();
	div.GetObject().УстановитьПометкуУдаления(true, true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
EndFunction

//--- Tools ---
procedure divisionToXml(div, xml, dataMap)
	xml.WriteStartElement("Division");
	xml.WriteAttribute("oid",  String(div.ref.UUID()));
	xml.WriteAttribute("pid",  String(div.Родитель.ref.UUID()));
	
	wap.writeTag(xml,"Name", div.Наименование);
	
	//if dataMap["contact"] = true then
	//	wap.writeTag(xml, "FullName", div.НаименованиеПолное);
	//endif;
		
	if dataMap["1c"] = true then
		wap.writeTag(xml, "ID",  String(div.Код));
	endif;
	
	xml.WriteEndElement(); 	
endprocedure



