﻿//*** Статьи затрат ***//
function itemsGET(Request)
try
	xml = wap.newXmlResponse();
	
	select = Справочники.СтатьиЗатрат.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true or select.Предопределенный = true 
		then
			continue;
		endif;
		
		attr = wap.getExtraAttribute(select.ref, "ImportDate");
		if attr = undefined then
			continue;
		endif;
	  		
		xml.WriteStartElement("ExpenseItem");
		xml.WriteAttribute("oid", String(select.ref.UUID()));
    xml.WriteAttribute("id", select.Код);
		xml.WriteAttribute("kind", String(select.ВидРасходовНУ));
		xml.WriteText(select.Наименование);
		xml.WriteEndElement(); 	
		
	enddo;                              
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function itemsPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	iter = wap.createXmlIterator(Request);
	if iter.tag <> "Index" then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	resultXml = wap.newXmlResponse();

	while wap.iterNextXmlElement(iter) do
		createOrUpdateExpenseItem(iter, resultXml);
	enddo;		
	
	return wap.doneResponse(resultXml); //"<OK/>");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure createOrUpdateExpenseItem(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("ExpenseItem");
  if iter.tag <> "ExpenseItem" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);	
	
try	
	oid = new UUID(iter.attributes.oid);
	ref = Справочники.СтатьиЗатрат.ПолучитьСсылку(oid);
	isNew = wap.isEmptyRef(ref);
	if isNew then
		obj = Справочники.СтатьиЗатрат.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
		obj.ВидРасходовНУ = Перечисления.ВидыРасходовНУ.ПрочиеРасходы;
	else
		obj = ref.GetObject();
	endif;	
		
	obj.Наименование = iter.text;
	//if iter.attributes.Property("inn") = true then
	//	obj.ИНН = iter.attributes.inn;
	//endif;	

	obj.Write();
	
	wap.setImportDate(ref, String(ТекущаяДата()));
	
	resultXml.WriteText(?(isNew, "создан", "обновлен"));
	resultXml.WriteEndElement(); // ExpenseItem
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // ExpenseItem
endtry;
endprocedure

//*** Затраты (приход услуг) **/
function expensesGET(Request)
try
	xml = wap.newXmlResponse();
	
	oid = Request.URLParameters["*"];
	if oid = "" or oid = "/index" then
		select = Документы.ПоступлениеТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			procType = wap.getProcType(select.ref);
			if procType = undefined or procType = "ProcessStockSupplyImprest" then  // только затраты
				continue;
			endif;
			xml.WriteStartElement("Doc");
			xml.WriteAttribute("oid",  String(select.ref.UUID()));
			procOID = wap.getProcOID(select.ref);
			if procOID <> undefined then
				xml.WriteAttribute("proc_oid",  String(procOID));
			endif;
			xml.WriteAttribute("proc_type",  procType);
    	xml.WriteAttribute("number",  select.Номер);
			xml.WriteAttribute("date",  wap.dateToISOString(select.Дата));
			xml.WriteEndElement(); 	
		enddo;   
	elsif oid = "/list" then
		select = Документы.ПоступлениеТоваровУслуг.Выбрать();
		while select.next()
		do
			if select.ПометкаУдаления = true OR select.Проведен = false then
				continue;
			endif;
			procType = wap.getProcType(select.ref);
			if procType = undefined or procType = "ProcessStockSupplyImprest" then  // только затраты
				continue;
			endif;
			expensesToXml(select, xml);
		enddo;                              
	else
		try
			oid = new UUID(Mid(oid,2));
		except
			return wap.errorResponse(400, "Параметром должен быть OID документа!");
		endTry;	
		ref = Документы.ПоступлениеТоваровУслуг.ПолучитьСсылку(oid);
		if wap.isEmptyRef(ref) then
			return wap.errorResponse(404, "Элемент с указанным OID не найден.");
 		endif;
		expensesToXml(ref, xml);
	endif;
	
	return wap.doneResponse(xml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function expensesPUT(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;

	resultXml = wap.newXmlResponse();
	
	expensesCreate(xmlIter, resultXml);
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function expensesPOST(Request)
try
	if wap.ensureXmlRequest(Request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
	
	xmlIter = wap.createXmlIterator(Request);
	if xmlIter.tag <> "List" then
		raise("Ошибка формата XML запроса: " + xmlIter.tag);
	endif;
	resultXml = wap.newXmlResponse();
	
	while wap.iterNextXmlElement(xmlIter) do
	 	expensesCreate(xmlIter, resultXml);
		if xmlIter.tag <> undefined then // пропуск нужен при ошибке или уже существует
			while wap.iterNextXmlElement(xmlIter, "Doc") do
			enddo;
		endif;
	 enddo;
	
	return wap.doneResponse(resultXml);
except
	return wap.exceptionResponse(ErrorInfo());
endTry;
endFunction
function expensesDELETE(request)
try
	try
		oid = Request.URLParameters["*"];
		oid = new UUID(Mid(oid,2));
	except
		return wap.errorResponse(400, "Параметром должен быть OID документа!");
	endTry;	
	
	oid = new UUID(oid);
	ref = Документы.ПоступлениеТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endFunction
procedure expensesCreate(xmlIter, resultXml)
	iter = xmlIter;
	
  resultXml.WriteStartElement("Doc");
  if iter.tag <> "Doc" then
		raise("Ошибка формата XML запроса: " + iter.tag);
	endif;
	if iter.attributes.Property("oid") = false then
		raise("Ошибка формата XML запроса: oid не указан!");
	endif;
	resultXml.WriteAttribute("oid",  iter.attributes.oid);
	procType = undefined;
	if iter.attributes.Property("type") = true then
		procType = iter.attributes.type;
		resultXml.WriteAttribute("proc_type",  procType);
	endif;
	
try	
	oid = new UUID(iter.attributes.oid);
	docRef = Документы.ПоступлениеТоваровУслуг.ПолучитьСсылку(oid);
	if wap.isEmptyRef(docRef) then
		doc = Документы.ПоступлениеТоваровУслуг.СоздатьДокумент();
		doc.УстановитьСсылкуНового(docRef); 
	else
		procOID = wap.getProcOID(docRef);
		if procOID <> undefined then
			resultXml.WriteAttribute("proc_oid",  String(procOID));
		endif;
		wap.writeTag(resultXml, "Number",  docRef.Номер);
    wap.writeTag(resultXml, "Date",  wap.dateToISOString(docRef.Дата));
		wap.writeTag(resultXml, "Result", "уже имеется, пропущен");		
		resultXml.WriteEndElement(); // Doc
		return;
	endif;	

	//doc.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
	doc.ВалютаДокумента = Справочники.Валюты.НайтиПоКоду(643);
	//doc.Организация = wap.getCurrentUserOrganization();
	doc.СпособЗачетаАвансов = Перечисления.СпособыЗачетаАвансов.Автоматически;
	doc.СчетУчетаРасчетовПоАвансам = ПланыСчетов.Хозрасчетный.РасчетыПоАвансамВыданным; //60.02 - по умолчанию
	//doc.Склад = wap.getDefaultDepot();
	doc.ВидОперации = Перечисления.ВидыОперацийПоступлениеТоваровУслуг.Услуги;

	
	writeMode = undefined;
	item = undefined;
	while wap.iterNextXmlElement(iter, "Doc") do
		if iter.tag = "Date" then
			doc.Дата = wap.dateFromISOString(iter.text);
		elsif iter.tag = "ProcOID" then
      procOID =  iter.text;
		elsif iter.tag = "ProcType" then
    	procType =  iter.text;
		elsif iter.tag = "ProcNumber" then
      procNumber =  iter.text;
		elsif iter.tag = "inDocNumber" then
      doc.НомерВходящегоДокумента =  iter.text;
		elsif iter.tag = "inDocDate" then
      doc.ДатаВходящегоДокумента =  wap.dateFromISOString(iter.text);
		elsif iter.tag = "OurFirm" then
			doc.Организация = wap.getOrganization(iter.text);	
		elsif iter.tag = "Covenantor" then	
			doc.Контрагент = wap.getCovenantor(iter.attributes.oid, iter.attributes.kind, iter.text);
			doc.ДоговорКонтрагента = wap.getCovenantorDefaultOrder(doc.Контрагент, Перечисления.ВидыДоговоровКонтрагентов.СПоставщиком);
		elsif iter.tag = "ExpenseItem" then
			expItemRef = wap.getExpenseItem(iter.text);
			item = doc.Услуги.Добавить();
			item.Субконто1 = expItemRef;
			item.СубконтоНУ1 = item.Субконто1;
			item.СчетЗатрат = wap.accountFromString("44.01");
			item.СчетЗатратНУ = wap.accountFromString("44.01");
			//item.Номенклатура = stockRef;
			item.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			item.Количество = 1;
		elsif iter.tag = "DocTotal" then
			doc.СуммаДокумента = Number(iter.text);
      item.Цена = doc.СуммаДокумента;
			item.Сумма = doc.СуммаДокумента;
		elsif iter.tag = "NdsRate" then      
      item.СтавкаНДС =  wap.enumFromString(Перечисления.СтавкиНДС, iter.text);
		elsif iter.tag = "SummaNDS" then
      item.СуммаНДС =  Number(iter.text);
		elsif iter.tag = "Note" then
			doc.Комментарий = iter.text;
		elsif iter.tag = "Debit" then
			item.СчетЗатрат = wap.accountFromString(iter.text); 
			item.СчетЗатратНУ = item.СчетЗатрат
		elsif iter.tag = "Credit" then	
			doc.СчетУчетаРасчетовСКонтрагентом = wap.accountFromString(iter.text); // ПланыСчетов.Хозрасчетный.
		elsif iter.tag = "WriteMode" then	
			writeMode = iter.text;
		endif;
	enddo;
	
	doc.УстановитьНовыйНомер();
	//doc.УстановитьВремя();
	check = doc.ПроверитьЗаполнение();
	if writeMode = undefined or writeMode = "Write" then
		doc.Write(РежимЗаписиДокумента.Запись);
		writeMode = "только записан";
	elsif writeMode = "Posting" then	
		doc.Write(РежимЗаписиДокумента.Проведение);
		writeMode = "записан, проведен";
	else
		raise "Режим записи документа [" + writeMode + "] не известен!";
	endif;
	
	if procOID <> undefined then
		wap.setProcOID(docRef, procOID);
		resultXml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	if procType <> undefined then
		wap.setProcType(docRef, procType);
	endif;
	if procNumber <> undefined then
		wap.setProcNumber(docRef, procNumber);
	endif;
	wap.setImportDate(docRef, String(ТекущаяДата()));
	
	wap.writeTag(resultXml, "Number",  doc.Номер);
  wap.writeTag(resultXml, "Date",  wap.dateToISOString(doc.Дата));	
	wap.writeTag(resultXml, "Result", writeMode);
	resultXml.WriteEndElement(); // Doc
except
	wap.exceptionXml(ErrorInfo(), resultXml);
	resultXml.WriteEndElement(); // Doc
endtry;
	
endprocedure
procedure expensesToXml(obj, xml)
	xml.WriteStartElement("Doc");
	xml.WriteAttribute("oid",  String(obj.ref.UUID()));
	procOID = wap.getProcOID(obj.ref);
	if procOID <> undefined then
		xml.WriteAttribute("proc_oid",  String(procOID));
	endif;
	procType = wap.getProcType(obj.ref);
	if procType <> undefined then
		xml.WriteAttribute("proc_type",  procType);
	endif;

	wap.writeTag(xml, "Number",  obj.Номер);
	wap.writeTag(xml, "Date", wap.dateToISOString(obj.Дата));
	procNumber = wap.getProcNumber(obj.ref);
	if procNumber <> undefined then
		wap.writeTag(xml, "ProcNumber", procNumber);
	endif;
	wap.writeTag(xml, "inDocNumber",  obj.НомерВходящегоДокумента);
	wap.writeTag(xml, "inDocDate",  wap.dateToISOString(obj.ДатаВходящегоДокумента));
	if wap.isEmptyRef(obj.Контрагент) = false then
		xml.WriteStartElement("Covenantor");
		xml.WriteAttribute("oid",  String(obj.Контрагент.ref.UUID()));
		xml.WriteText(obj.Контрагент.Наименование);
		xml.WriteEndElement(); // Covenantor
	elsif	obj.ПринятоОт <> "" then
		wap.writeTag(xml, "Payer", obj.ПринятоОт);
	endif; 
	wap.writeTag(xml, "OurFirm", obj.Организация.ref.UUID());
  wap.writeTag(xml, "DocTotal", obj.СуммаДокумента);
	wap.writeTag(xml, "Note", obj.Комментарий);
	
	xml.WriteStartElement("Expenses");
	for each item in obj.Услуги do
		xml.WriteStartElement("Expense");
		xml.WriteAttribute("item",  String(item.Субконто1.ref.UUID()));
		wap.writeTag(xml,"Summa", item.Сумма);
		wap.writeTag(xml,"NDS", item.СтавкаНДС);
		wap.writeTag(xml,"SummaNDS", item.СуммаНДС);
		xml.WriteEndElement(); // Expense
	enddo;	
	xml.WriteEndElement(); // Expenses
	
	xml.WriteEndElement(); // Doc	
endprocedure

