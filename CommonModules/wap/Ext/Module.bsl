﻿// --- XML ---
procedure writeTag(xmlWriter, tag, text) export
	xmlWriter.WriteStartElement(tag);
	xmlWriter.WriteText(String(text));
	xmlWriter.WriteEndElement();
endProcedure
function ensureXmlRequest(Request) export
	ctype = Request.Headers["Content-Type"];
	if ctype = undefined or StrStartWith(ctype,"text/xml") = false then
		return false;
	endif;
	return true;
endfunction
// Можно указать либо запрос, либо строку.
function createXmlIterator(httpServiceRequest, xmlString = null) export 
	iter = new Structure();
	xr =  new XMLReader();
	if httpServiceRequest = undefined then
		xr.SetString(xmlString);
	else
		xr.SetString(httpServiceRequest.GetBodyAsString());
	endif;
	iter.Insert("xmlReader", xr);
	iter.Insert("tag", undefined);
	iter.Insert("attributes", new Structure());
	iter.Insert("text", undefined);
	iter.Insert("hasChilds", false);
	wap.iterNextXmlElement(iter);
	return iter;
endfunction
function iterNextXmlElement(xmlIterator, untilCloseTag = null) export
 	x = xmlIterator;
 	xr = x.xmlReader;
 		
	x.attributes.Clear();
 	if xr.NodeType = XMLNodeType.StartElement then
		x.tag = xr.LocalName;

		if xr.AttributeCount() > 0 then
			while xr.ReadAttribute() do
				x.attributes.Insert(xr.LocalName, xr.Value);
			enddo;
		endif;
	else
		x.tag = undefined;
	endif;
	x.text = undefined;
	x.hasChilds = false;
 
	while xr.Read() do
		if xr.NodeType = XMLNodeType.StartElement then
			if x.tag <> undefined then
				x.hasChilds = true;
				return true;
			endif;
			x.tag = xr.LocalName;
			if xr.AttributeCount() > 0 then
				while xr.ReadAttribute() do
					x.attributes.Insert(xr.LocalName, xr.Value);
				enddo;
			endif;
		elsif xr.NodeType = XMLNodeType.Attribute then
			x.attributes.Insert(xr.LocalName, xr.Value);
		elsif xr.NodeType = XMLNodeType.EndElement then
			if xr.LocalName = untilCloseTag then
				x.tag = undefined;
				return false;
			elsif x.tag <> undefined then
				return true;
			endif;
		elsif xr.NodeType = XMLNodeType.Text or xr.NodeType = XMLNodeType.CDATASection then	
			x.text = xr.Value;
		endif; 
	enddo;
	return false;
endfunction
function printXmlIterator(iter) export
	res = wap.xmlIteratorToString(iter);
	while wap.iterNextXmlElement(iter) do
		 wap.xmlIteratorToString(iter, res);
	enddo;
	return res;			
endfunction
// --- ToFromString ---
function dateToISOString(date) export
	if TypeOf(date) <> Type("Date") then
		raise "[" + String(date) + "] не является объектом типа <Дата>!";
	endif;
	return Format(date, "ДФ=ггггММдд") + "T" + Format(date, "ДФ=ЧЧммсс") ;	
endfunction
function dateFromISOString(str) export
	if TypeOf(str) <> Type("String") then
		raise "[" + String(str) + "] не является объектом типа <String>!";
	endif;
	if IsBlankString(str) then
		return Date("00010101");
	else
		return Date(StrReplace(str, "T", ""));
	endif;
endfunction
function moneyToString(summa) export
	if TypeOf(summa) <> Type("Number") then
		raise "[" + String(summa) + "] не является объектом типа <Number>!";
	endif;	
	return StrReplace(StrReplace(TrimAll(String(summa)), Chars.NBSp, ""),",",".");
endFunction
function xmlIteratorToString(iter, str = "") export
	str = str + "tag:" + iter.tag + " hasChilds:" + String(iter.hasChilds);
	str = str + " attrs:[";
	for each a in iter.attributes do
		str = str + a.Key + "=""" + a.Value + """ ";
	enddo;
	str = str + "] text:" + iter.text + Chars.LF;
	return str;
endfunction
function enumFromString(enum, value) export
	if TypeOf(enum) = Type("String") then
		enumName = enum;
	else
		enumName = enum[0].Метаданные().Имя;
		//raise "Значение enum не является перечислением или строкой!";
	endif;
	for each e in Метаданные.Перечисления[enumName].ЗначенияПеречисления do                      			
		if value = e.Имя or value = e.Синоним then
			return Перечисления[enumName][e.Имя];
		endif;
	enddo;
	raise "Значение " + value + " не найдено в " + String(enumName);
endFunction
function enumToString(enum) export
	enumName = enum.Метаданные().Имя;
  index = Перечисления[enumName].Индекс(enum);
	return Метаданные.Перечисления[enumName].ЗначенияПеречисления[index].Имя;
endFunction
// --- HTTP ---
function requestArgs(Request) export
	par = new Map();
	for each kv in Request.QueryOptions do
		par[kv.Key] = kv.Value;
	enddo;
	return par;
endFunction
function newXmlResponse(openRootTag = "Response") export
	xml = new XMLWriter;
	xml.SetString("UTF-8");
	xml.WriteXMLDeclaration();
	if IsBlankString(openRootTag) = false then
		xml.WriteStartElement(openRootTag);
	endif;
	return xml;
endFunction
function doneResponse(xml, code = 200, closeRootTag = true) export
	itXml = TypeOf(xml) = Type("XMLWriter");
	Response = new HTTPServiceResponse(code);
	if itXml then
		if closeRootTag = true then
			xml.WriteEndElement();
		endif;
		Response.Headers.Insert("Content-type", "text/xml");
		Response.SetBodyFromString(xml.Close());
	else
		Response.SetBodyFromString(xml);
	endif;
	Return Response;
endFunction
// Возвращает HTTPServiceResponse с указанным кодом ошибки и описанием.
function errorResponse(code, message = "") export  
	Response = new HTTPServiceResponse(code);
	if IsBlankString(message) = false then
		Response.Headers.Insert("Content-type", "text/xml");
		xml = new XMLWriter;
		xml.SetString("UTF-8");
		xml.WriteXMLDeclaration();
		xml.WriteStartElement("Error");
		writeTag(xml, "Message", message);
		xml.WriteEndElement(); //"Error"
		Response.SetBodyFromString(xml.Close());
	endif;
	return Response;
endfunction
// Записывает в указанный XMLWriter описание исключения в виде xml.
// Если xmlWriter не создан, создает его.
// Возвращает xmlWriter
function exceptionXml(errInfo, xmlWriter = undefined) export
	if xmlWriter = undefined then
  	xml = new XMLWriter;
		xml.SetString("UTF-8");
		xml.WriteXMLDeclaration();
	else
		xml = xmlWriter;
	endif;
	xml.WriteStartElement("Error");
		writeTag(xml, "Message", errInfo.Description);
		writeTag(xml, "ModuleName", errInfo.ModuleName);
		writeTag(xml, "LineNumber", String(errInfo.LineNumber));
		writeTag(xml, "SourceLine", errInfo.SourceLine);
		if errInfo.Cause <> undefined then
			errInfo = errInfo.Cause;
			while errInfo <> undefined do
				xml.WriteStartElement("Trace");
				writeTag(xml, "Message", errInfo.Description);
				writeTag(xml, "ModuleName", errInfo.ModuleName);
				writeTag(xml, "LineNumber", String(errInfo.LineNumber));
				writeTag(xml, "SourceLine", errInfo.SourceLine);
				xml.WriteEndElement(); //"Trace"	
				errInfo = errInfo.Cause;
			enddo;
		endif;
	xml.WriteEndElement(); //"Error"	
  return xml;
endfunction
// Возвращает HTTPServiceResponse с описанием исключения в виде xml.
function exceptionResponse(errInfo) export  
	Response = new HTTPServiceResponse(500);
	Response.Headers.Insert("Content-type", "text/xml");
	xml = exceptionXml(errInfo); 
	Response.SetBodyFromString(xml.Close());
	return Response;
endfunction
/// Функция удаления. Возвращает HTTPServiceResponse
/// Параметры:
///  request - объект запроса (HTTPServiceRequest)
///  manager - менеджер объектов (например, Справочники.ВидыНоменклатуры)
///  tagName - имя тега элемента
function doDeleteByXml(request, manager, tagName) export
	try
	if wap.ensureXmlRequest(request) = false then
		return wap.errorResponse(415, "Поддерживаются только XML запросы!");
	endif;
		
	oid = new UUID(request.URLParameters["oid"]);
	ref = manager.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		return wap.errorResponse(404, "Элемент с указанным OID не найден.");
  endif;
	
	iter = wap.createXmlIterator(request);
	if iter.tag <> "Delete" or iter.hasChilds = false  then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
	wap.iterNextXmlElement(iter);
  if iter.tag <> tagName then
		return  wap.errorResponse(422, "Ошибка формата XML запроса: " + iter.tag);
	endif;
  if iter.attributes.oid <> request.URLParameters["oid"] then
		return wap.errorResponse(400, "OID в запросе отличается от OID в теле XML!");
	endif;
	
	ref.GetObject().УстановитьПометкуУдаления(true, true);

	return wap.doneResponse("OK");
except
	return wap.exceptionResponse(ErrorInfo());
endtry;
endfunction

// --- Common methods ---
// Возвращает организацию текущего пользователя
function getCurrentUserOrganization() export
	user = ПользователиИнформационнойБазы.ТекущийПользователь();
	mainOrg = Справочники.Организации.ОрганизацияПоУмолчанию(user);
	if mainOrg = undefined or wap.isEmptyRef(mainOrg) then
		raise "Не удалось определить организацию по умолчанию для пользователя '" + user.Name + "'!";
	endif;
	return mainOrg;
endfunction
// Возвращает ссылку на организацию по OID фирмы из WAP
function getOrganization(oid, throwIfNotFound = true) export
	if TypeOf(oid) <> Type("String") then
		oid = String(oid);
	endif;
	if oid = undefined or StrLen(oid) <> 36 then
		raise "Не верное значение OID!";
	endif;
	select = Справочники.Организации.Выбрать();
	while select.next()
	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
			continue;
		endif;
		if oid = wap.getOID(select.ref)	then
			return select.ref;
	  endif;
	enddo;
	if throwIfNotFound = true then
		raise "Не удалось определить организацию c OID [" + oid + "]!";
	endif;
	return undefined;
endfunction
// Возвращает ссылку на контрагента. Если нет, создает.
function getCovenantor(oid, kind, name) export
	if TypeOf(oid) = Type("String") then
		oid = new UUID(oid);
	endif;
	
	ref = Справочники.Контрагенты.ПолучитьСсылку(oid);
	if wap.isEmptyRef(ref) then
		obj = Справочники.Контрагенты.СоздатьЭлемент();
		obj.УстановитьСсылкуНового(ref); 
		
		if kind = "Company" then
			obj.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо;
		else  // Individual Person
			obj.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
   	endif;
		//if iter.attributes.Property("inn") = true then
		//	obj.ИНН = iter.attributes.inn;
		//endif;	
	  obj.Наименование = name;
	  obj.Write();
	endif;
	
	return ref;
endfunction
function getCovenantorDefaultOrder(Контрагент, ВидДоговора) export
	org = wap.getCurrentUserOrganization();
	Query = new Query;
	Query.Text = "
	  |SELECT
		|    Ссылка
		|FROM Справочник.ДоговорыКонтрагентов
		|WHERE Организация = &Организация и Владелец = &Контрагент и ВидДоговора = &ВидДоговора и Номер = ""0""
		|";
	Query.SetParameter("Организация", org);
	Query.SetParameter("Контрагент", Контрагент);
	Query.SetParameter("ВидДоговора", ВидДоговора);
	select = Query.Execute().Select();
	if select.Next() then
		return select.Ссылка;
	else
		obj = Справочники.ДоговорыКонтрагентов.СоздатьЭлемент();
		obj.ВидДоговора = ВидДоговора;
		obj.Номер = "0";
		obj.Дата = ТекущаяДата();
		obj.Организация = org;
		obj.Владелец = Контрагент;
		obj.Наименование = "Без договора";
		obj.ВалютаВзаиморасчетов = Справочники.Валюты.НайтиПоКоду(643);
		obj.Write();
		return obj.ref;
  endif;
	return Справочники.ДоговорыКонтрагентов.ПустаяСсылка();
endfunction
function getDefaultDepot() export
	select = Справочники.Склады.Выбрать();
	while select.next()	do
		if select.ПометкаУдаления = true or select.ЭтоГруппа = true then
			continue;
		endif;
		return select.ref;
	enddo;
	raise "Склад по умолчанию не найден!";	
endfunction
function accountFromString(str) export
	acc = ПланыСчетов.Хозрасчетный.НайтиПоКоду(str);
	if acc = undefined then
		raise "Не найден счет - " + str;
	endif;
	return acc;
endfunction
/// Вовзращает ссылку на статью затрат
function getExpenseItem(oid) export
	if TypeOf(oid) = Type("String") then
		oid = new UUID(oid);
	endif;
	
	expItemRef = Справочники.СтатьиЗатрат.ПолучитьСсылку(oid);
	
	if wap.isEmptyRef(expItemRef) then
		raise "Статья затрат с OID = [" + String(oid) + "] не найдена!";
	endif;
	if expItemRef.IsFolder = true then
		raise "Статья затрат с OID = [" + String(oid) + "] является папкой!";
	endif;
	return expItemRef;
endfunction
// --- is Tools ---
function isRef(refOrObj) export
	XMLИмяТипа = XMLТипЗнч(refOrObj).ИмяТипа;
	if Найти(XMLИмяТипа, "Ref.") > 0 then
  	return true;
	endif;
	return false;
endfunction
function isEmptyRef(ref) export
	if ref = undefined then
		return true;
	endif;	
	return IsBlankString(ref.ВерсияДанных); // ref.IsEmpty()
endfunction
function isEmptyUUID(uuid) export
	if uuid = undefined then
		return true;
	endif;	
	empty = new UUID("00000000-0000-0000-0000-000000000000");	
	return uuid = empty;
endfunction
function isEmptyDate(date) export
	return date = Дата(1, 1, 1, 0, 0, 0);
endfunction
function isEmptyString(str) export
	return str = undefined or str = "";
endfunction

// --- Доп. реквизиты ---
function getExtraAttribute(objectRef, attrib) export
													 
	if TypeOf(attrib) = Type("String") then
		propStore = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения;
		prop = propStore.НайтиПоНаименованию(attrib, true);
		if prop = undefined OR prop.IsEmpty() then
			raise "Не найдено свойство " + attrib + "!"; 
		endif;
	elsif attrib = undefined OR attrib.IsEmpty() then
		raise "Не указано свойство !";
	else	
		prop = attrib;
	endif;
	
	if objectRef = undefined then
		raise "objectRef = undefined";
	endif;
	if wap.isRef(objectRef) = false then
		objectRef = objectRef.Ссылка();
	endif;	
	if wap.isEmptyRef(objectRef) then
		raise "Пустая ссылка!";
	endif;	

	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	ТаблицаСвойств.Свойство КАК Свойство,
		|	ТаблицаСвойств.Значение КАК Значение,
		|	"""" КАК ТекстоваяСтрока,
		|	ТаблицаСвойств.Объект КАК ВладелецСвойств
		|ИЗ
		|	РегистрСведений.ДополнительныеСведения КАК ТаблицаСвойств
		|ГДЕ
		|	ТаблицаСвойств.Объект В (&ОбъектыСоСвойствами)
		|	И ТаблицаСвойств.Свойство В (&Свойства)";
	Запрос.Параметры.Вставить("ОбъектыСоСвойствами", objectRef);
	Запрос.Параметры.Вставить("Свойства", prop);
	
	Результат = Запрос.Выполнить().Выгрузить();
	if Результат.Количество() > 0  then
		return Результат[0].Значение;
	endif;
	return undefined;	
КонецФункции
function setExtraAttribute(objectRef, propName, value) export
	if Константы.ИспользоватьДополнительныеРеквизитыИСведения.Получить() <> true then
		raise  "ИспользоватьДополнительныеРеквизитыИСведения отключено!";
	endif;
		
	if objectRef = undefined then
		raise "objectRef = undefined";
	endif;
	if wap.isRef(objectRef) = false then
		objectRef = objectRef.Ссылка();
	endif;	
	if wap.isEmptyRef(objectRef) then
		raise "setExtraAttribute: Пустая ссылка!";
	endif;	
	
	propStore = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения;
	prop = propStore.НайтиПоНаименованию(propName, true);
	if prop = undefined OR prop.IsEmpty() then
		raise "Не найдено свойство " + propName + "!"; 
	endif;
	
	МенеджерЗаписи = РегистрыСведений.ДополнительныеСведения.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Объект = objectRef;
	МенеджерЗаписи.Свойство = prop;
	МенеджерЗаписи.Значение = String(value);
		
	МенеджерЗаписи.Записать(Истина);
	
	//value = wap.getExtraAttribute(objectRef, "OID"); 
	
endfunction
function getOID(objectRef) export
	return  getExtraAttribute(objectRef, "OID");
endfunction
function setOID(objectRef, oidValue) export
	setExtraAttribute(objectRef,"OID", oidValue);
endFunction
function getProcOID(objectRef) export
	return  getExtraAttribute(objectRef, "ProcOID");
endfunction
function setProcOID(objectRef, oidValue) export
	setExtraAttribute(objectRef,"ProcOID", oidValue);
endFunction
function getProcType(objectRef) export
	return  getExtraAttribute(objectRef, "ProcType");
endfunction
function setProcType(objectRef, stringValue) export
	setExtraAttribute(objectRef,"ProcType", stringValue);
endFunction
function getProcNumber(objectRef) export
	return  getExtraAttribute(objectRef, "ProcNumber");
endfunction
function setProcNumber(objectRef, stringValue) export
	setExtraAttribute(objectRef,"ProcNumber", stringValue);
endFunction
function getImportDate(objectRef) export
	return  getExtraAttribute(objectRef, "ImportDate");
endfunction
function setImportDate(objectRef, stringValue) export
	setExtraAttribute(objectRef,"ImportDate", stringValue);
endFunction

//???
function default(name) export
	res = ХранилищеОбщихНастроек.Загрузить("WAP", name);
	if name = "Организация" then
		if res = undefined then
			raise "Defaults: Организация не задана.";
		endif;
		res = Справочники.Организации.ПолучитьСсылку(new UUID(res));
		if wap.isEmptyRef(res) or res.IsFolder = true then
			raise "Defaults: Организация не найдена.";
		endif;
	elsif name = "Склад" then   
		if res = undefined then
			raise "Defaults: Склад не задан.";
		endif;
		res = Справочники.Склады.ПолучитьСсылку(new UUID(res));
		if wap.isEmptyRef(res) or res.IsFolder = true then
			raise "Defaults: Склад не найден.";
		endif;
	elsif name = "ВидОплатЭквайринг" then   
		if res = undefined then
			raise "Defaults: Вид оплат 'Эквайринг' не задан.";
		endif;
		res = Справочники.ВидыОплатОрганизаций.ПолучитьСсылку(new UUID(res));
		if wap.isEmptyRef(res) or res.IsFolder = true then
			raise "Вид оплат 'Эквайринг' не найден.";
		endif;
	elsif name = "ВидОплатСертификат" then   
		if res = undefined then
			raise "Defaults: Вид оплат 'Сертификат' не задан.";
		endif;
		res = Справочники.ВидыОплатОрганизаций.ПолучитьСсылку(new UUID(res));
		if wap.isEmptyRef(res) or res.IsFolder = true then
			raise "Вид оплат 'Сертификат' не найден.";
		endif;
	endif;
	return res;
endFunction
function convertPaymentKind(refOrEnum) export
	if  TypeOf(refOrEnum) = Type("String") then
		if refOrEnum = "CreditCard" then return default("ВидОплатЭквайринг");
		elsif  refOrEnum = "Certificate" then return default("ВидОплатСертификат");
		else raise "Вид оплаты [" + refOrEnum + "] не определен!";
		endif;
	else
		if refOrEnum = default("ВидОплатЭквайринг") then return "CreditCard"; 
		elsif refOrEnum = default("ВидОплатСертификат") then return "Certificate";
		else raise "Вид оплаты [" + refOrEnum.Наименование + "] не определен!";
		endif;
	endif;
endFunction
	




